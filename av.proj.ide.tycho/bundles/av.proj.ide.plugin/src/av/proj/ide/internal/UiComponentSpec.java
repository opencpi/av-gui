/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.internal;

public class UiComponentSpec implements Comparable {
	String componentName;
	String oasReference = null;
	String owdReference;
	String fileName;
	String displayName = null;
	String packageId;
	String projectPackageId;
	public UiComponentSpec() {};
	public UiComponentSpec(String libId, String comp, String filePathname) {
		componentName = comp;
		packageId = libId;
		String[] split = filePathname.split("/");
		fileName = split[split.length -1];
		oasReference = packageId + "." + componentName;
		owdReference = fileName.substring(0, fileName.length() - 4);
	}
	public String getProjectPackageId() {
		return projectPackageId;
	}
	public String getComponentName() {
		return componentName;
	}
	public String getOasReference() {
		return oasReference;
	}
	public String getOwdReference() {
		return owdReference;
	}
	public String getFileName() {
		return fileName;
	}
	public static boolean isComponentFile(String filename) {
		if(filename.endsWith("-spec.xml") || filename.endsWith("_spec.xml")) {
			return true;
		}
		return false;
	}
	public String getDisplayName() {
		if(displayName == null) {
			if(oasReference == null) return null;
			
			if(packageId.isEmpty()) {
				return componentName;
			}
			StringBuilder sb = new StringBuilder(componentName);
			sb.append(" (");
			sb.append(packageId);
			sb.append(")");
			displayName = sb.toString();
			//System.out.println(displayName);
		}
		return displayName;
	}
	private int myHashcode;
	private boolean hashNotBuild = true;
	private String equalsString = null;
	private String getEqualsString() {
		if(equalsString == null) {
			if(packageId != null) {
				equalsString = packageId;
			}
			else {
				equalsString = projectPackageId;
			}
			equalsString += componentName; 
		}
		return equalsString; 
	}
	
	@Override
	public boolean equals(Object other) {
		if(super.equals(other) == true) return true;
		if(other instanceof UiComponentSpec){
			String others = ((UiComponentSpec)other).getEqualsString();
			return others.equals(getEqualsString());
		}
		return false;
	}
	@Override
	public int hashCode() {
		if(hashNotBuild) {
			String me = getEqualsString();
			myHashcode = me.hashCode(); 
			hashNotBuild = false;
		}
		return myHashcode;
	}
	private String myString = null;
	private String format = "%s %s, %s";
	@Override
	public int compareTo(Object other) {
		if(super.equals(other) == true) return 0;
		
		UiComponentSpec othr = (UiComponentSpec) other;
		String me = getDisplayName();
		return me.compareTo(othr.getDisplayName());
	}
	
}
