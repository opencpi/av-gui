/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetModel;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.json.simple.JSONObject;

import av.proj.ide.envServices.OpenCpiEnvSupportMethods;
import av.proj.ide.internal.UiComponentSpec;
import av.proj.ide.internal.XmlFileFilter;

/***
 * This class 
 */
public class JsonProjectParser {
	public static JSONObject getProjectJson(JSONObject jsonObject, String projectId) {
    	JSONObject projectsObj = (JSONObject) jsonObject.get("projects");
		return (JSONObject) projectsObj.get(projectId);
	}
	@SuppressWarnings("unchecked")
	public static Collection<String> getLibraries(JSONObject projectObject) {
    	JSONObject librariesObj = (JSONObject) projectObject.get("libraries");
		return librariesObj.keySet();
	}
	
	public static JSONObject getLibraryJson(JSONObject projectObj, String libraryId) {
	   	JSONObject librariesObj = (JSONObject) projectObj.get("libraries");
	   	JSONObject library = (JSONObject) librariesObj.get(libraryId);
		return library;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, UiComponentSpec> getSpecsInLibrary(JSONObject libraryObj, String libraryId) {
	   	JSONObject componentsObj = (JSONObject) libraryObj.get("components");
		Set<String> components = componentsObj.keySet();
		TreeMap<String, UiComponentSpec> comps = new TreeMap<String, UiComponentSpec>();
		for(String comp : components) {
			String filePathname = (String)componentsObj.get(comp);
			UiComponentSpec spec = new UiComponentSpec(libraryId, comp, filePathname);
			comps.put(comp, spec);
		}
		return comps;
	}

	public static TreeMap<String, EnvironmentProject> getOpenCpiProjects(JSONObject jsonObject) {
		// This gets the registered projects. Unregistered Opencpi projects are not
		// picked up here.
		TreeMap<String, EnvironmentProject> ocpiProjects = null;
		if(jsonObject != null) {
        	JSONObject projectsObj = (JSONObject) jsonObject.get("projects");

        	if(projectsObj != null) {
   	        @SuppressWarnings("unchecked")
    			Set<String> projects = projectsObj.keySet();
    	        ocpiProjects = new TreeMap<String, EnvironmentProject>();
    	        for(String key : projects) {
    	        	EnvironmentProject project = new EnvironmentProject(key, (JSONObject) projectsObj.get(key));
    	        	ocpiProjects.put(key, project);
      	        }
   		    }
 		}
	    return ocpiProjects;
	}

	@SuppressWarnings("unchecked")
	public
	static void populateSecondTierData(JSONObject projectContents, AssetModelData project) {
		JSONObject projObject = (JSONObject)projectContents.get("project");
		if(projObject == null) return;
		
		EnvironmentProject proj = project.projectLocation;
		AssetModelData specs = getTopLevelSpecs(proj);
		if(specs != null) project.addChild(specs);
		
		List<String> xmlApps = getXmlOnlyApps(proj);
		JSONObject topObject = (JSONObject)projObject.get("applications");
		if(topObject != null) {
			AssetModelData folder = OcpiAssetFactory.createTierAsset(OpenCPICategory.applications, proj);
			project.addChild(folder);
			populateNameDirectoryPairs(folder, topObject, OpenCPICategory.application);
			addXmlApps(xmlApps, folder, proj);
			folder.setPopulated();
		}
		else {
			if(xmlApps != null) {
				AssetModelData folder = OcpiAssetFactory.createTierAsset(OpenCPICategory.applications, proj);
				project.addChild(folder);
				addXmlApps(xmlApps, folder, proj);
				folder.setPopulated();
			}
		}
		topObject = (JSONObject)projObject.get("libraries");
		if(topObject != null) {
			Set<String> libraries = topObject.keySet();
			TreeMap<String, AssetModelData> complibs = new TreeMap<String, AssetModelData>();
			TreeMap<String, AssetModelData> hdllibs = new TreeMap<String, AssetModelData>();
			boolean hasComponentsLibrary = false;
			if(! libraries.isEmpty()) {
				for(String packageId : libraries) {
					String dir = (String)topObject.get(packageId);
					String[] split = dir.split("/");
					String lib = split[split.length -1];
					
					AssetModelData library = null;
					if(lib.startsWith("adapters") || lib.startsWith("cards") || lib.startsWith("devices") ) {
						library =  
							OcpiAssetFactory.createLibraryAsset(OpenCPICategory.library, lib, packageId, dir, proj);
						hdllibs.put(lib, library);
					}
					else if(packageId.contains("platform")) {
						continue;
					}
					else {
						if(lib.startsWith("compo")){
							library = OcpiAssetFactory.
									createLibraryAsset(OpenCPICategory.componentsLibrary, lib, packageId, dir, proj);
							project.addChild(library);
							library.parent = project;
							AssetModelData dummyAsset = new AssetModelData("", proj,OpenCPICategory.worker);
							library.addChild(dummyAsset);
							hasComponentsLibrary = true;
						}
						else {
							library = OcpiAssetFactory.
									createLibraryAsset(OpenCPICategory.library, lib, packageId, dir, proj); 
						}
						complibs.put(lib, library);
					}
				}
			}
			if(complibs.size() > 1 || (complibs.size() == 1 && hasComponentsLibrary == false) ) {
				AssetModelData folder =  OcpiAssetFactory.createTierAsset(OpenCPICategory.componentsLibraries, proj);
				project.addChild(folder);
				folder.parent = project;
				folder.setPopulated();
				for(AssetModelData library: complibs.values()) {
					AssetModelData dummyAsset = new AssetModelData("", proj,OpenCPICategory.worker);
					library.addChild(dummyAsset);
					library.parent = folder;
					folder.addChild(library);
				}
			}
			
			for(String key: hdllibs.keySet()) {
				AssetModelData library = hdllibs.get(key);
				AssetModelData dummyAsset = new AssetModelData("", proj,OpenCPICategory.worker);
				OpenCPICategory cat = null;
				switch(key) {
				case "cards":
					cat = OpenCPICategory.cards;
					break;
				case "devices":
					cat = OpenCPICategory.devices;
					break;
				case "adapters":
					cat = OpenCPICategory.adapters;
					break;
					
				}
				library.category = cat;
				library.addChild(dummyAsset);
				library.parent = project;
				project.addChild(library);
			}
		}
		
		// Assemblies data does not appear in the 2nd tier report. Just put one
		// in place.
		AssetModelData assemblies = OcpiAssetFactory.createTierAsset(OpenCPICategory.assemblies, proj);
		assemblies.parent = project;
		project.addChild(assemblies);
		AssetModelData assembly = new AssetModelData("", proj,OpenCPICategory.assembly);
		assemblies.addChild(assembly);
		
		topObject = (JSONObject)projObject.get("platforms");
		if(topObject != null) {
			JSONObject hdl = (JSONObject)topObject.get("hdl");
			if(hdl != null) {
				Set<String> assets = hdl.keySet();
				if(assets.size() > 0) {
					AssetModelData folder = OcpiAssetFactory.createTierAsset(OpenCPICategory.platforms, proj);
					project.addChild(folder);
					populateNameDirectoryPairs(folder, hdl, OpenCPICategory.platform);
					folder.setPopulated();
				}
			}
		}
		project.setPopulated();
	}

	private static void addXmlApps(List<String> xmlApps, AssetModelData folder, EnvironmentProject proj) {
		for(String name : xmlApps) {
			String directory = folder.getDirectory();
			AssetModelData asset = new AssetModelData(name, proj, OpenCPICategory.xmlapp);
			asset.parent = folder;
			asset.directory = directory;
			asset.assetFolder = "applications";
			asset.assetFileName = name;
			folder.addChild(asset);
		}
	}
	private static List<String> getXmlOnlyApps(EnvironmentProject location) {
		File projectFolder = new File(location.projectPath);
		if (projectFolder != null && projectFolder.exists()) {
			File appsDir = new File(projectFolder, "applications");
			if (appsDir != null && appsDir.exists()) {
				FileFilter filter = new XmlFileFilter();
				File[] files = appsDir.listFiles(filter);
				
				if(files.length == 0) return null;
				
				ArrayList<String> apps = new ArrayList<String>();
				for(File appfile : files) {
					apps.add(appfile.getName());
				}
				return apps;
			}
		}
		return null;
	}
	private static boolean isSpecName(String specName) {
		if(specName.toLowerCase().endsWith("spec.xml"))
			return true;
		
		return false;
	}
	
	private static AssetModelData getTopLevelSpecs(EnvironmentProject location) {
		File projectFolder = new File(location.projectPath);
		if (projectFolder != null && projectFolder.exists()) {
			File specsDir = new File(projectFolder, "specs");
			if (specsDir != null && specsDir.exists()) {
				AssetModelData folder = OcpiAssetFactory.createTierAsset(OpenCPICategory.topLevelSpecs, location);
				FileFilter filter = new XmlFileFilter();
				File[] files = specsDir.listFiles(filter);
				ArrayList<AssetModelData> protocols = new ArrayList<AssetModelData>();
				ArrayList<AssetModelData> components = new ArrayList<AssetModelData>();
				ArrayList<AssetModelData> addList;
				for(File specfile : files) {
					String name = specfile.getName();
					OpenCPICategory cat;
					if(isSpecName(name)) {
						cat = OpenCPICategory.component;
						addList = components;
					}
					else {
						cat = OpenCPICategory.protocol;
						addList = protocols;
					}
					AssetModelData asset = new AssetModelData(name, location, cat);
					asset.parent = folder;
					asset.directory = specsDir.getAbsolutePath();
					asset.assetFolder = "specs";
					asset.assetFileName = name;
					addList.add(asset);
				}
				folder.childList = components;
				folder.childList.addAll(protocols);
				folder.setPopulated();
				return folder;
			}
		}
		
		return null;
	}
	
	public static void populateAsset(AssetModelData asset, JSONObject pObject) {
		if(pObject == null) return;
		JSONObject projObject = (JSONObject)pObject.get("project");
		
		JSONObject topObject = null;
		
		switch(asset.category) {
		case componentsLibrary:
		case library:
		case adapters:
		case cards:
		case devices:
			topObject = (JSONObject)projObject.get("libraries");
			JSONObject library = (JSONObject)topObject.get(asset.qualifiedName);
			populateLibrary(asset, library);
			break;
		case assemblies:
			topObject = (JSONObject)projObject.get("assemblies");
			populateNameDirectoryPairs(asset, topObject, OpenCPICategory.assembly);
			break;
		case primitives:
			topObject = (JSONObject)projObject.get("primitives");
			populateNameDirectoryPairs(asset, topObject, OpenCPICategory.primitive);
			break;
		default:
		}
		
	}

	@SuppressWarnings("unchecked")
	private static void populateNameDirectoryPairs(AssetModelData asset, JSONObject jObject, OpenCPICategory cat) {
		if(jObject == null) return;
		Set<String> assets = jObject.keySet();
		if(assets.size() > 0) {
			
			for(String name : assets) {
				String directory = (String)jObject.get(name);
				AssetModelData child = OcpiAssetFactory.
						createLeafAsset(cat, asset.projectLocation, name, directory);
				child.parent = asset;
				asset.addChild(child);
			}
			asset.setPopulated();
		}
	}

	private static void populateLibrary(AssetModelData asset, JSONObject library) {
		String libraryPath = asset.directory;
		String specsDirectory = libraryPath + "/specs";
		JSONObject topObject = (JSONObject)library.get("components");
		AssetModelData folder = null;
		if(topObject != null && ! topObject.isEmpty()) {
			folder = OcpiAssetFactory.createTierAsset(OpenCPICategory.specs, asset.projectLocation);
			folder.parent = asset;
			folder.directory = specsDirectory;
			asset.addChild(folder);
			populateComponents(folder, topObject);
		}
		File specsDir = new File(specsDirectory);
		TreeSet<String> protocolFilenames = new TreeSet<String>();
		OpenCpiEnvSupportMethods.getProtocols(specsDir, protocolFilenames);
		addProtocols(folder, protocolFilenames);
			
		topObject = (JSONObject)library.get("workers");
		populateNameDirectoryPairs(asset, topObject, OpenCPICategory.worker);
		topObject = (JSONObject)library.get("tests");
		populateNameDirectoryPairs(asset, topObject, OpenCPICategory.test);
	}

	@SuppressWarnings("unchecked")
	private static void populateComponents(AssetModelData folder, JSONObject jObject) {
		if(jObject == null) return;
		Set<String> assets = jObject.keySet();
		if(assets.size() > 0) {
			for(String name : assets) {
				String directory = (String)jObject.get(name);
				String[] split = directory.split("/");
				String filename = split[split.length -1];
				AssetModelData child = OcpiAssetFactory.
						createComponentAsset(OpenCPICategory.component, folder.projectLocation, folder.directory, name, filename);
				child.parent = folder;
				folder.addChild(child);
			}
			folder.setPopulated();
		}
	}
	private static void addProtocols(AssetModelData folder, TreeSet<String> protocolFilenames) {
		for(String protocolName : protocolFilenames) {
			AssetModelData asset = OcpiAssetFactory.
					createComponentAsset(OpenCPICategory.protocol, folder.projectLocation, folder.directory, protocolName, protocolName);
			asset.parent = folder;
			folder.addChild(asset);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Set<UiComponentSpec>> getProjectComponents(JSONObject projectCompObj) {
		JSONObject libs = (JSONObject)projectCompObj.get("libraries");
		TreeMap<String, Set<UiComponentSpec>> projectComps = new TreeMap<String, Set<UiComponentSpec>>();
		Set<String> ids = libs.keySet();
		for(String libId : ids) {
			TreeSet<UiComponentSpec> comps = new TreeSet<UiComponentSpec>();
			projectComps.put(libId, comps);
			JSONObject libObj = (JSONObject)libs.get(libId);
			JSONObject compObj = (JSONObject)libObj.get("components");
			Set<String> cids = compObj.keySet();
			for(String comp : cids) {
				String filePathname = (String) compObj.get(comp);
				UiComponentSpec spec = new UiComponentSpec(libId, comp, filePathname);
				comps.add(spec);
			}
		}
		return projectComps;
	}
	@SuppressWarnings("unchecked")
	public static Map<String, Map<String, UiComponentSpec>> getProjectObjects(JSONObject componentsJson) {
		JSONObject projsObject = (JSONObject)componentsJson.get("projects");
		if(projsObject == null) return null;
		Map<String, Map<String, UiComponentSpec>> allComps = new TreeMap<String, Map<String, UiComponentSpec>>();
		Set<String> ids = projsObject.keySet();
		for(String projId : ids) {
			JSONObject projObject = (JSONObject)projsObject.get(projId);
			Map<String, Set<UiComponentSpec>> comps = getProjectComponents(projObject);
			TreeMap<String, UiComponentSpec> projComps = new TreeMap<String, UiComponentSpec>();
			for(Set<UiComponentSpec> libComps: comps.values()) {
				for(UiComponentSpec comp : libComps) {
					projComps.put(comp.getDisplayName(), comp);
				}
			}
			allComps.put(projId,projComps);
		}
		return allComps;
	}
	@SuppressWarnings("unchecked")
	public static Map<String, Map<String, Set<UiComponentSpec>>> getAllProjectComponents(JSONObject componentsJson) {
		JSONObject projsObject = (JSONObject)componentsJson.get("projects");
		if(projsObject == null) return null;
		
		Map<String, Map<String, Set<UiComponentSpec>>> allComps = new TreeMap<String, Map<String, Set<UiComponentSpec>>>();
		Set<String> ids = projsObject.keySet();
		for(String projId : ids) {
			JSONObject projObject = (JSONObject)projsObject.get(projId);
			Map<String, Set<UiComponentSpec>> comps = getProjectComponents(projObject);
			allComps.put(projId, comps);
		}
		return allComps;
	}

}
