/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetModel;

import java.util.List;

/**
 * This class is the base class of the UI asset data model. It identifies an
 *  asset, where it is, what it is, and aspects of how to build it 
 *  held in the OcpiAssetCategory enumeration. It is the key object that moves 
 *  through the build system and status monitor.
 *   
 * Note that from a build perspective, a project, project subfolders are
 * considered assets as well as individual applications, component, assemblies,
 * etc.
 * 
 * This class can also used as a lookup key in a map so it can be readily found
 * (equals and hashcode are implemented).
 */
public class AngryViperAsset {
	
	public EnvironmentProject projectLocation;
	public OpenCPICategory category;
	
	// Name is OpenCPI framework name for the asset.
	public String assetName;
	// qualified name is a package id or package id + the asset name.
	public String qualifiedName = null;
	
	public String libraryPath = null;

	public boolean buildable = true;
	public Object assetDetails;
	
	//public AngryViperAsset parent;
	
	protected List<String> buildString = null;
	public void setLocation(EnvironmentProject location) {
		this.projectLocation = location;
	}

	// Purposely  placed in package scope so the factory is used to create them.
	AngryViperAsset(){}
	AngryViperAsset(String assetName, EnvironmentProject location, OpenCPICategory category){
		this.assetName = assetName;
		this.projectLocation = location;
		this.category = category;
	}
	public AngryViperAsset(AngryViperAsset other){
		this.assetName = other.assetName;
		this.projectLocation = other.projectLocation;
		this.category = other.category;
		this.libraryPath = other.libraryPath;
		this.buildString = other.buildString;
	}
	
	public String getProjectsDisplayName() {
		if(category == OpenCPICategory.project) {
			return qualifiedName;
		}
		return assetName;
	}
	public String getOpsDisplayName() {
			return qualifiedName;
	}

	private int myHashcode;
	private boolean hashNotBuild = true;
	@Override
	public boolean equals(Object asset) {
		// The expectation is typically it will be
		// the same object or not.
		
		if(super.equals(asset) == true) return true;
		
		if(asset instanceof AngryViperAsset)
		{
			AngryViperAsset other = (AngryViperAsset)asset;
			return this.category == other.category &&
					this.assetName.equals(other.assetName) &&
					//this.projectLocation.projectName.equals(other.projectLocation.projectName) &&
					this.projectLocation.projectPath.equals(other.projectLocation.projectPath) &&
					(this.libraryPath == null ? true : this.libraryPath.equals(other.libraryPath));
		}
		return false;
	}
	@Override
	public int hashCode() {
		if(hashNotBuild) {
			myHashcode = assetName.hashCode() + projectLocation.projectPath.hashCode() + category.name().hashCode();
//			if(buildName != null) {
//				myHashcode += buildName.hashCode();
//			}
			if(libraryPath != null) {
				myHashcode += libraryPath.hashCode();
			}
			hashNotBuild = false;
		}
		return myHashcode;
	}
	private String myString = null;
	private String format = "%s %s, %s";
	
	@Override
	public String toString() {
		if(myString == null) {
			myString = String.format(format, assetName, category.getFrameworkName(), projectLocation.projectPath);
		}
		return myString;
	}
}
