/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetModel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.TreeItem;

/**
 * This is a container class used to accumulate an asset and its children.
 * The project view obtains these as projects when it initializes and constructs
 * the project tree.  Change updates are also sent in this container.
 */
public class AssetModelData extends AngryViperAsset {

	String directory;
	String assetFolder = null;
	String assetFileName = null;
	String filePath = null;
	
	ArrayList<AssetModelData> childList = null;
	
	AssetModelData parent;
	TreeItem assetUiItem;
	boolean  isPopulated = false;
	
	Object   data = null;
	
//	protected List<String> buildString = null;
//  Actually this needs to be a fresh copy due to different build configs or have build and clean strings.
	
	public void setData(Object rawData) {
		data = rawData;
	}
	public Object getData() {
		return data;
	}
	
	public String getAssetName() {
		return assetName;
	}
//	public String getProjectsDisplayName() {
//		if(category == OpenCPICategory.project) {
//			return qualifiedName;
//		}
//		else if (assetName != null){
//			return assetName;
//		}
//		else if (buildName != null){
//			return buildName;
//		}
//		else {
//			return "??";
//		}
//	}
//	public String getOpsDisplayName() {
//		if(qualifiedName != null) {
//			return qualifiedName;
//		}
//		else if (assetName != null){
//			return assetName;
//		}
//		else {
//			return "??";
//		}
//	}
	public String getQualifiedName() {
		return qualifiedName;
	}
	
	public TreeItem getAssetUiItem() {
		return assetUiItem;
	}
	public void setAssetUiItem(TreeItem assetUiItem) {
		this.assetUiItem = assetUiItem;
	}
	public OpenCPICategory getCategory() {
		return category;
	}
	public AssetModelData getParent() {
		return parent;
	}
	public String getDirectory() {
		return directory;
	}
	public String getAssetFolder() {
		if(assetFolder == null) {
			String[] split = directory.split("/");
			assetFolder = split[split.length -1];
		}
		return assetFolder;
	}
	public String getAssetFileName() {
		return assetFileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public List<AssetModelData> getChildList() {
		return childList;
	}
	public boolean hasChildren() {
		return childList != null;
	}
	
	public void addChild(AssetModelData child) {
		if(childList == null) {
			childList =  new ArrayList<AssetModelData>();
		}
		childList.add(child);
	}
	
	public AssetModelData(AngryViperAsset asset) {
		childList = new ArrayList<AssetModelData>();
	}
	public AssetModelData(String name, EnvironmentProject projectLocation, OpenCPICategory cat) {
		super(name, projectLocation, cat);
	}
	public AngryViperAsset getAsset() {
		return this;
	}

	public boolean isPopulated() {
		return isPopulated;
	}
	public void setPopulated() {
		isPopulated = true;
	}
	public void clearChildren() {
		if(childList == null) return;
		childList.clear();
	}

}
