/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetModel;

import av.proj.ide.wizards.internal.CreateAssetFields;

/***
 * The AngryViperAsset has become a significant object.  It identifies the asset, has build
 * information and it is used to lookup UI model information.  This factory intends to isolate
 * AngryViperAsset construction to one place so it is constructed consistently.
 */

// TODO: This was work in progress, never completed or verified;
public class OcpiAssetFactory {

	public static AssetModelData createOcpiAsset(String name, String libraryName, OpenCPICategory cat, EnvironmentProject projectLocation){
		
		AssetModelData asset = null;
		return asset;
	}

	public static AssetModelData createParentOcpiAsset(AngryViperAsset asset, String parentName){
		AssetModelData nextParent = null;
		if(asset == null)
			return null;
		
		switch(asset.category) {
		default: 
			nextParent = createOcpiAsset(asset.projectLocation.openCpiProjectName, null, OpenCPICategory.project, asset.projectLocation);
			break;
			
		case project:
			nextParent = null;
			break;

			
		case application:
		case xmlapp:
			nextParent = createOcpiAsset(null, null, OpenCPICategory.applications ,asset.projectLocation);
			break;
		case primitive:
			nextParent = createOcpiAsset(null, null, OpenCPICategory.primitives ,asset.projectLocation);
			break;
		case platform: 
			nextParent = createOcpiAsset(null, null, OpenCPICategory.platforms ,asset.projectLocation);
			break;
		case assembly:
			nextParent = createOcpiAsset(null, null, OpenCPICategory.assemblies ,asset.projectLocation);
			break;

		// This is not used yet.  Creating the assets is readily supported by the asset wizard however,
		// so these cases are here to match things up. It needs to be thought out.
		case card:
		case device:
		case hdlTest:
			//nextParent = createOcpiAsset(null, null, asset.category ,asset.projectLocation);
			break;

		case component:
		case protocol:
			if(OpenCPICategory.componentsLibrary.getFrameworkName().equals(asset.libraryPath)) {
				nextParent = createOcpiAsset(OpenCPICategory.specs.getFrameworkName(), asset.libraryPath, OpenCPICategory.specs, asset.projectLocation);
			}
			else if(asset.libraryPath.equals(OpenCPICategory.specs.getFrameworkName())) {
				nextParent = createOcpiAsset(OpenCPICategory.topLevelSpecs.getFrameworkName(), null, OpenCPICategory.topLevelSpecs, asset.projectLocation);
			}
			else {
				nextParent = createOcpiAsset(OpenCPICategory.specs.getFrameworkName(), asset.libraryPath, OpenCPICategory.specs, asset.projectLocation);
			}
			
			break;
		case worker:
		case test:
			if(OpenCPICategory.componentsLibrary.getFrameworkName().equals(asset.libraryPath)) {
				nextParent = createOcpiAsset(asset.libraryPath, null, OpenCPICategory.componentsLibrary, asset.projectLocation);
			}
			else {
				nextParent = createOcpiAsset(asset.libraryPath, null, OpenCPICategory.library ,asset.projectLocation);
			}
			break;
			
		case library:
			nextParent = createOcpiAsset(null, null, OpenCPICategory.componentsLibraries ,asset.projectLocation);
			break;
			
		case specs:
			if(OpenCPICategory.componentsLibrary.getFrameworkName().equals(asset.libraryPath)) {
				nextParent = createOcpiAsset(asset.libraryPath, null, OpenCPICategory.componentsLibrary, asset.projectLocation);
			}
			else {
				nextParent = createOcpiAsset(asset.libraryPath, null, OpenCPICategory.library ,asset.projectLocation);
			}
			break;
		
		case topLevelSpecs:
			nextParent = createOcpiAsset(asset.projectLocation.openCpiProjectName, null, OpenCPICategory.project, asset.projectLocation);
			break;
		}
		
		return nextParent;
	}

	public static AssetModelData createLeafAsset(OpenCPICategory cat, EnvironmentProject project, String name, String directory) {
		AssetModelData asset = new AssetModelData(name, project, cat);
		String projectPackageId = project.packageId;
		asset.qualifiedName = projectPackageId + "." +  name;
		asset.assetFolder = name;
		asset.directory = directory;
		
		switch(cat) {
		case xmlapp:	
			asset.assetFolder = "applications";
			// TODO: Need to deal with xml name;
		case component:
		case protocol:
			String[] split = directory.split("/");
			String filename = split[split.length -1];
			asset.assetName = filename;
			asset.assetFileName = filename;
			asset.assetFolder = "specs";
			asset.buildable = false;
			break;

		case card:
			// This represents a card worker
		case device:
			// This represents a device worker
		case hdlTest:
		case worker:
		case test:
			// Creating a dummy asset
			if(directory != null && ! directory.isEmpty()) {
				asset.libraryPath = directory.substring(0,directory.length() - name.length() -1);
			}
			break;
			
			default:
		}
		
		return asset;
	}

	public static AssetModelData createTierAsset(OpenCPICategory cat, EnvironmentProject project) {
		AssetModelData asset = null;
		String projectPackageId = project.packageId;
		String folder = cat.getFrameworkName();
		String qualifiedName = projectPackageId + "."  + cat.getFrameworkName();
		asset = new AssetModelData(cat.getFrameworkName(), project, cat);
		asset.qualifiedName = qualifiedName;
		asset.assetFolder = folder;
		asset.directory = project.projectPath +"/" + folder;
		
		switch(cat) {
		case primitives:
		case platforms :
		case assemblies:
			asset.assetFolder = "hld/" + folder;
			break;
			
		case cards:
		case devices:
			asset.qualifiedName = projectPackageId + "." + cat.getFrameworkName();
			break;
			
		// These represent non-buildable folders
		case specs:
		case topLevelSpecs:
			asset.buildable = false;
			break;
		default:
			break;
		}
		return asset;
	}
	
	public static AssetModelData createProjectAsset(EnvironmentProject proj) {
		AssetModelData asset = new AssetModelData(proj.openCpiProjectName, proj, OpenCPICategory.project);
		asset.directory = proj.projectPath;
		asset.assetFolder = proj.projectFolder;
		asset.qualifiedName = proj.packageId;
		return asset;
	}

	public static AssetModelData createNewAsset(CreateAssetFields assetElem, EnvironmentProject project) {
		AssetModelData asset = null;
		String libPath = null;
		String assetName = assetElem.getName();
		OpenCPICategory cat = assetElem.getType();
		
		switch(assetElem.getType()) {
		case application:
			if(assetElem.isXmlOnly()) {
				asset = createLeafAsset(OpenCPICategory.xmlapp, project, assetName+".xml", project.projectPath + "/applications");
				
			}
			else {
				asset = createLeafAsset(cat, project, assetName, project.projectPath + "/applications/" + assetName);
			}
			break;
		case assembly:
			asset = createLeafAsset(cat, project, assetName, project.projectPath + "/hdl/assemblies/" + assetName);
			break;
		case component:
		case protocol:
			String libraryName = assetElem.getLibraryName();
//			public static AssetModelData createComponentAsset(OpenCPICategory cat, EnvironmentProject project,
//					String directory, String name, String filename) {
			
			String fileName; 
			if(cat == OpenCPICategory.component) {
				fileName = assetName+"-spec.xml";
			}
			else {
				fileName = assetName+"-prot.xml";
			}
			String directory;
			if(assetElem.isTopLevelSpec()) {
				directory =  project.projectPath + "/specs";
			}
			else if(libraryName.startsWith("comp")) {  // should use the framework name from OpenCPICategory.
				libPath = project.projectPath + "/components";
				directory =  project.projectPath + "/components/specs";
			}
			else {
				libPath =  project.projectPath + "/components/"+ libraryName;
				directory =  libPath + "/specs";
			}
			asset = createComponentAsset(cat, project, directory, assetName, fileName);
			asset.libraryPath = libPath;
			break;
		case library:
			break;
		case platform:
			asset = createLeafAsset(cat, project, assetName, project.projectPath + "/hdl/platforms/" + assetName);
			break;
		case primitive:
			libPath = project.projectPath + "/hdl/primitives";
			asset = createLeafAsset(cat, project, assetName, libPath + "/" + assetName);
			asset.libraryPath = libPath;
			break;
		case project:
			break;
		case test:
		case worker:
			String libName = assetElem.getLibraryName();
			String filName = assetName+".xml";
			String dir;
			if(libName.startsWith("comp")) {  // should use the framework name from OpenCPICategory.
				libPath =  project.projectPath + "/components";
				dir =  libPath + "/" + assetName;
				dir =  project.projectPath + "/components/" + assetName;
			}
			else {
				libPath =  project.projectPath + "/components/"+ libName;
				dir =  libPath + "/"  + assetName;
			}
			asset = createLeafAsset(cat, project, assetName, dir);
			asset.libraryPath = libPath;
			asset.assetFileName = filName;
			break;
		default:
			break;
		
		}
		return null;
	}

	public static AssetModelData createLibraryAsset(OpenCPICategory cat, String name, String packageId, String directory,
			EnvironmentProject project) {
		AssetModelData asset = new AssetModelData(name, project, cat);
		asset.directory = directory;
		asset.qualifiedName = packageId;
		asset.assetFolder = name;
		return asset;
	}

	public static AssetModelData createComponentAsset(OpenCPICategory cat, EnvironmentProject project,
			String directory, String name, String filename) {
		AssetModelData asset = new AssetModelData(filename, project, cat);
		String projectPackageId = project.packageId;
		asset.qualifiedName = projectPackageId + "." +  name;
		asset.assetFileName = filename;
		asset.assetFolder = "specs";
		asset.directory = directory;
		asset.buildable = false;
		return asset;
	}


}
