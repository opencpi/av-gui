/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.swt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.EnvironmentProject;
import av.proj.ide.assetModel.OpenCPICategory;
import av.proj.ide.assetServices.ProjectModelController;
import av.proj.ide.assetServices.ProjectViewSupport;
import av.proj.ide.assetServices.ProjectViewUpdater;
import av.proj.ide.assetServices.SyncLock;
import av.proj.ide.avps.internal.AvpsResourceManager;
import av.proj.ide.avps.internal.BuildTargetSelections;
import av.proj.ide.avps.internal.ProjectBuildService;
import av.proj.ide.avps.internal.ProjectBuildService.ProvideBuildSelections;
import av.proj.ide.avps.internal.SelectionsInterface;
import av.proj.ide.avps.internal.UserBuildSelections;
import av.proj.ide.internal.OcpidevVerb;

public class ProjectViewSwtDisplay extends Composite implements SelectionsInterface, ProjectViewUpdater {
	private ProjectViewServices controller;
	private ProjectViewSupport service;
	
	Tree projectsTree;
	ArrayList<TreeItem> projectTrees;
	private Composite headerArea;
	
	Button addSelectionsButton;
	Button synchronizeButton;
	private Composite gap;
	private ProjectImages projectImages;
	//private  AckModelDataUpdate	ackUpdate = null;
	Composite parent;
	BuildTargetSelections lastSelections = new BuildTargetSelections();
	
	public Tree getProjectPanel() {
		return projectsTree;
	}
	
	public ProjectImages getUiImages() {
		return projectImages;
	}
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ProjectViewSwtDisplay(Composite parent, int style) {
	super(parent, style);
		this.parent = parent;
		controller = new ProjectViewServices();

		projectTrees = new ArrayList<TreeItem>();
		AvpsResourceManager.getInstance().registerSelectionProviders((SelectionsInterface) this);
		
		headerArea = new Composite(this, SWT.BORDER_SOLID);
		headerArea.setLayout(new GridLayout(1, false) );
		
		Composite buttons = new Composite(this, SWT.NONE);
		buttons.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		synchronizeButton = new Button(buttons, SWT.PUSH);
		synchronizeButton.setText("Refresh");
		synchronizeButton.setToolTipText("Refresh assets from the file system.");
		
		gap = new Composite(this, SWT.NONE);
		gap.setLayout(new GridLayout(1, false) );
		
		projectsTree = new Tree(this, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		
		
		// ===================================================================
		//                          Panel Layout 
		// ===================================================================
		FormLayout layout = new FormLayout();
		layout.marginTop = 5;
		layout.marginBottom = 5;
		this.setLayout(layout);
		
		FormData data;
		
		// Header Panel - top and right of buildPanel
		data = new FormData();
		data.top = new FormAttachment(0, 5);
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.bottom  = new FormAttachment(0, 25);
		headerArea.setLayoutData(data);	

		// Buttons
		data = new FormData();
		data.top = new FormAttachment(headerArea, 5);
		data.left = new FormAttachment(0, 20);
		data.right = new FormAttachment(0, 200);
		buttons.setLayoutData(data);
	
		// Gap
		data = new FormData();
		data.top = new FormAttachment(buttons, 5);
		data.left = new FormAttachment(0, 10);
		data.right = new FormAttachment(100, -10);
		gap.setLayoutData(data);

		
		// Selection Panel
		data = new FormData();
		data.top = new FormAttachment(gap, 15);
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.bottom  = new FormAttachment(100, -5);
		projectsTree.setLayoutData(data);
		
		addSelectionMenu();
		projectsTree.addMouseListener(new MouseAdapter() {
			@Override
		    public void mouseDoubleClick(MouseEvent e) {

				TreeItem[] items = projectsTree.getSelection();
				if(items.length == 1) {
					AssetModelData asset = (AssetModelData) items[0].getData();
					
					switch(asset.getCategory()) {
					case application:
					case xmlapp:
					case assembly:
					case component:
					case platform:
					case protocol:
					case test:
					case worker:
						controller.openEditor(asset, parent);
						break;
						
					default:
						break;
					
					}
				}

		    }			
		}
		);
		projectsTree.addListener(SWT.Expand, expandListener);
	}
	
	public void setPanelColorScheme(AvColorScheme colorScheme) {
		headerArea.setBackground(colorScheme.getPrimary());
		gap.setBackground(colorScheme.getSecondary());
	}
	
	private SyncLock updateLock = new SyncLock();
	public synchronized void loadModelData(ProjectViewSupport  assetServices, ProjectImages images ) {
		updateLock.acquire();
		projectImages = images;
		service = assetServices;
		service.registerProjectView(this);
		
		// This will block until the service finishes loading.
		
		TreeMap<String, ProjectModelController> projects = assetServices.getInitialProjects();
		for(ProjectModelController project: projects.values()) {
			addProject(project.getProjectModel());
		}
		updateLock.release();
		
		// Integration with the old update approach
//		ackUpdate = AngryViperAssetService.getInstance().registerProjectModelRefresh(new ModelDataUpdate(){
//			public void processChangeSet(Set<AssetModelData> removedAssets, Set<AssetModelData> newAssets) {
//				processChanges(removedAssets, newAssets);
//			}
//		});
	}

	private Listener expandListener = new Listener() {
		@Override
		public void handleEvent(Event e) {
			Widget item = e.item;
			Object data = item.getData();
			if(data instanceof AssetModelData) {
				AssetModelData asset = (AssetModelData)data;
				if(! asset.isPopulated()) {
					// Hack used to allow the node to expand; current child is a dummy entry
					// and needs to be removed.
					AssetModelData currentChild = asset.getChildList().get(0);
					ProjectModelController ctrl = service.getModelController(asset);
					ctrl.populateAsset(asset);
					updateAssetChildren(asset);
					currentChild.getAssetUiItem().dispose();
				}
			}
		}
	};

	private SelectionAdapter buildAdapter = new SelectionAdapter() {
		
     	private ProjectBuildService srv = ProjectBuildService.getInstance();
    	
		public void widgetSelected(SelectionEvent event) {
			MenuItem item =  (MenuItem)event.widget;
			OcpidevVerb verb = (OcpidevVerb)item.getData();
			ProvideBuildSelections provider = ProjectBuildService.getInstance().getBuildselectionProvider();
			
			if(provider != null) {
				UserBuildSelections userSelections = provider.getBuildSelections();
				userSelections.verb = verb;
				if(userSelections.assetSelections.size() > 0 ) {
					userSelections.assetSelections.clear();
				}
				TreeItem[] sels = projectsTree.getSelection();
				for( int i=0; i < sels.length; i++) {
					AssetModelData asset = (AssetModelData)sels[i].getData();
					userSelections.assetSelections.add(asset);
				}
				srv.processBuildRequest(userSelections);
			}
		}
	};
	
	private SelectionAdapter fileSystemAdapter = new SelectionAdapter() {
		
		public void widgetSelected(SelectionEvent event) {
			MenuItem item =  (MenuItem)event.widget;
			String cmd = item.getText();
			TreeItem[] sels = projectsTree.getSelection();
			if(sels.length != 1)
				return;
			
			TreeItem selection = sels[0];
			AssetModelData asset = (AssetModelData)selection.getData();
			if(asset == null)
				return;

			switch(cmd) {
			case "open":
				controller.openEditor(asset, parent);
				break;
				
			case "delete asset":
				controller.deleteAsset(asset, parent);
				break;
				
			case "register project":
				controller.registerProject(asset, parent);
				break;

			case "unregister project":
				controller.unregisterProject(asset, parent);
				break;

			default:
				return;
			}
		}
	};
	
	private SelectionAdapter wizardAdapter = new SelectionAdapter() {
		
		public void widgetSelected(SelectionEvent event) {
			MenuItem item =  (MenuItem)event.widget;
			OpenCPICategory type = (OpenCPICategory)item.getData();
			
			TreeItem[] sels = projectsTree.getSelection();
			if((sels.length == 0 && type == null) || sels.length>1) {
				controller.putUpWizard();
			}
			else {
				TreeItem sel = sels[0];
				AssetModelData asset = (AssetModelData)sel.getData();
				controller.putUpSpecificWizard(type, asset);
			}
		}
	};
	
	private Menu menu = null;
	private MenuItem build, clean, open, delete;
	
	private void addSelectionMenu () {
	    menu = new Menu(projectsTree);
	    projectsTree.setMenu(menu);
	    
        MenuItem newItem = new MenuItem(menu, SWT.NONE);
	    
        newItem.setText("asset wizard");
        newItem.addSelectionListener(wizardAdapter);
        
        newItem = new MenuItem(menu, SWT.NONE);
        newItem.setText("build");
        newItem.setData(OcpidevVerb.build);
        newItem.addSelectionListener(buildAdapter);
        build = newItem;
        
        newItem = new MenuItem(menu, SWT.NONE);
        newItem.setText("clean");
        newItem.setData(OcpidevVerb.clean);
        newItem.addSelectionListener(buildAdapter);
        clean = newItem;
        
        newItem = new MenuItem(menu, SWT.NONE);
        newItem.setText("open");
        newItem.addSelectionListener(fileSystemAdapter);
        open = newItem;
        
        newItem = new MenuItem(menu, SWT.NONE);
        newItem.setText("delete asset");
        newItem.addSelectionListener(fileSystemAdapter);
        delete = newItem;

	    menu.addMenuListener(new MenuAdapter()
	    {
	    	@Override
	    	public void menuShown(MenuEvent e)
	        {
				TreeItem[] sels = projectsTree.getSelection();
				
				
	            MenuItem[] items = menu.getItems();
	            int number = items.length;
	            
	            // Remove all but the base menu items.  Now that
	            // register/unregister project is added there are
	            // issues with the use of different listeners.
				if(number > 5) {
					for(int i = 5; i < number; i++) {
						items[i].dispose();
					}
				}
	            
				if(sels.length == 0) {
					delete.setEnabled(false);
					build.setEnabled(false);
					clean.setEnabled(false);
					open.setEnabled(false);
					return;
				}
	            
	            if(sels.length > 1) {
					delete.setEnabled(false);
					open.setEnabled(false);
					return;
	            }
	            
	            // One item is selected
				
				delete.setEnabled(true);
				open.setEnabled(false);
				build.setEnabled(true);
				clean.setEnabled(true);
				TreeItem sel = sels[0];
				AssetModelData asset = (AssetModelData)sel.getData();
				OpenCPICategory cat = asset.category;
				MenuItem theItem = null;
				
				switch (cat) {
				case xmlapp:
					build.setEnabled(false);
					clean.setEnabled(false);
				case application:
					open.setEnabled(true);
				case applications:
					if(cat == OpenCPICategory.applications)
						delete.setEnabled(false);

					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.addSelectionListener(wizardAdapter);
			        theItem.setText("new application");
			        theItem.setData(OpenCPICategory.application);
					
					break;
				case assembly:
					open.setEnabled(true);
				case assemblies:
					if(cat == OpenCPICategory.assemblies)
						delete.setEnabled(false);

					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.addSelectionListener(wizardAdapter);
			        theItem.setText("new assembly");
			        theItem.setData(OpenCPICategory.assembly);
			        
					break;
				case card:
					
				case specs:
			        delete.setEnabled(false);
			        
			        theItem = new MenuItem(menu, SWT.NONE);
			        theItem.setText("new unit test");
			        theItem.setData(OpenCPICategory.test);
			        theItem.addSelectionListener(wizardAdapter);
			        
				case topLevelSpecs:
			        delete.setEnabled(false);
			        
					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.addSelectionListener(wizardAdapter);
			        theItem.setText("new component");
			        theItem.setData(OpenCPICategory.component);

			        theItem = new MenuItem(menu, SWT.NONE);
			        theItem.setText("new protocol");
			        theItem.setData(OpenCPICategory.protocol);
			        theItem.addSelectionListener(wizardAdapter);
			        break;
				
				case component:
					open.setEnabled(true);
					build.setEnabled(false);
					clean.setEnabled(false);
			        theItem = new MenuItem(menu, SWT.NONE);
			        theItem.setText("new worker");
			        theItem.setData(OpenCPICategory.worker);
			        theItem.addSelectionListener(wizardAdapter);
			        
					if(asset.getParent().category != OpenCPICategory.topLevelSpecs) {
				        theItem = new MenuItem(menu, SWT.NONE);
				        theItem.setText("new unit test");
				        theItem.setData(OpenCPICategory.test);
				        theItem.addSelectionListener(wizardAdapter);
					}
					break;
					
				case componentsLibrary:
				case componentsLibraries:
				case library:
					
				delete.setEnabled(false);
				
				theItem = new MenuItem(menu, SWT.NONE);
		        theItem.addSelectionListener(wizardAdapter);
		        theItem.setText("new component");
		        theItem.setData(OpenCPICategory.component);

		        theItem = new MenuItem(menu, SWT.NONE);
		        theItem.setText("new worker");
		        theItem.setData(OpenCPICategory.worker);
		        theItem.addSelectionListener(wizardAdapter);

		        theItem = new MenuItem(menu, SWT.NONE);
		        theItem.setText("new protocol");
		        theItem.setData(OpenCPICategory.protocol);
		        theItem.addSelectionListener(wizardAdapter);
		        
		        theItem = new MenuItem(menu, SWT.NONE);
		        theItem.setText("new unit test");
		        theItem.setData(OpenCPICategory.test);
		        theItem.addSelectionListener(wizardAdapter);
		        
					break;
				case device:
					break;
				case hdlTest:
					break;
				case platform:
					open.setEnabled(true);
				case platforms:
					if(cat == OpenCPICategory.platforms)
						delete.setEnabled(false);
					
					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.addSelectionListener(wizardAdapter);
			        theItem.setText("new platform");
			        theItem.setData(OpenCPICategory.platform);
			        
					break;
				case primitive:
				case primitives:
					if(cat == OpenCPICategory.primitives)
						delete.setEnabled(false);
					
					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.addSelectionListener(wizardAdapter);
			        theItem.setText("new primitive");
			        theItem.setData(OpenCPICategory.primitive);
					
					break;
				case project:
					// We've verified one item is selected in the view.  If the
					// menu had more the 6 selections, the selection past 6 have been
					// removed.  Now we know the user has selected a project and we want
					// to allow the user to unregister the project or register it.
					// Now we need to see if the project is registered or not and add
					// the appropriate selection.
					
					TreeItem selection = sels[0];
					AssetModelData project = (AssetModelData)selection.getData();
					EnvironmentProject projectInfo = project.projectLocation;
					if( projectInfo != null && projectInfo.isRegistered() ) {
						theItem = new MenuItem(menu, SWT.NONE);
				        theItem.setText("unregister project");
				        theItem.setData(OpenCPICategory.project);
				        theItem.addSelectionListener(fileSystemAdapter);
						
					}
					else {
						theItem = new MenuItem(menu, SWT.NONE);
				        theItem.setText("register project");
				        theItem.setData(OpenCPICategory.project);
				        theItem.addSelectionListener(fileSystemAdapter);
					}
						
					break;
				case protocol:
					open.setEnabled(true);
					build.setEnabled(false);
					clean.setEnabled(false);
					
					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.setText("new protocol");
			        theItem.setData(OpenCPICategory.protocol);
			        theItem.addSelectionListener(wizardAdapter);
					break;
				case test:
					open.setEnabled(true);
					break;
				case tests:
					if(cat == OpenCPICategory.tests)
						delete.setEnabled(false);
					
					break;
				case worker:
					open.setEnabled(true);
					
					theItem = new MenuItem(menu, SWT.NONE);
			        theItem.setText("new worker");
			        theItem.setData(OpenCPICategory.worker);
			        theItem.addSelectionListener(wizardAdapter);
					break;
					
				case cards:
				case devices:
					delete.setEnabled(false);
					break;
					
				default:
					break;
				}
	        }
	    });

	}
	// Meet the interface requirements.
	@Override
	public void addSelections(TreeItem[] items) {
		// do nothing.
		
	}
	@Override
	public TreeItem[] getSelections() {
		TreeItem[] selections = projectsTree.getSelection();
		return selections;
	}
	@Override
	public synchronized void asyncUpdateProject(AssetModelData project) {
		getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				updateProject(project);
			}
		});
	}
	
	@Override
	public void asyncAddProjects(TreeMap<String, AssetModelData> openCpiProjects) {
		updateLock.acquire();
		updateLock.release();
		getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				addProjects(openCpiProjects);
			}
		});
	}

	void addProjects (TreeMap<String, AssetModelData> projects) {
		for(AssetModelData project: projects.values()) {
			addProject(project);
		}
	}
	
	@Override
	public void addProject(AssetModelData project) {
		TreeItem projItem = new TreeItem(projectsTree, SWT.NONE);
		projItem.setText(project.getProjectsDisplayName());
		projItem.setImage(projectImages.getProject());
		projItem.setData(project);
		project.setAssetUiItem(projItem);
		updateAssetChildren(project);
	}
	
	public void updateProject(AssetModelData project) {
		TreeItem parentItem = project.getAssetUiItem();
		for(AssetModelData child : project.getChildList()) {
			TreeItem childItem = new TreeItem(parentItem, SWT.NONE);
			child.setAssetUiItem(childItem);
			childItem.setText(child.getProjectsDisplayName());
			childItem.setImage(projectImages.getImage(child.getCategory()));
			childItem.setData(child);
			updateAssetChildren(child);
		}
	}
	
	void updateAssetChildren(AssetModelData asset) {
		if(! asset.hasChildren()) return;
		TreeItem parentItem = asset.getAssetUiItem();
		for(AssetModelData child : asset.getChildList()) {
			TreeItem childItem = new TreeItem(parentItem, SWT.NONE);
			child.setAssetUiItem(childItem);
			childItem.setText(child.getProjectsDisplayName());
			childItem.setImage(projectImages.getImage(child.getCategory()));
			childItem.setData(child);
			if(child.hasChildren()) {
				updateAssetChildren(child);
			}
		}
	}
	
	@Override
	public void addNewAsset(AssetModelData asset) {
		if(asset.getParent() == null) {
			// hit the project tree item
			return;
		}
		TreeItem parent = asset.getParent().getAssetUiItem();
		if( parent != null && asset.getAssetUiItem() == null) {
			TreeItem newItem = new TreeItem(parent, SWT.NONE);
			asset.setAssetUiItem(newItem);
			newItem.setText(asset.getProjectsDisplayName());
			newItem.setImage(projectImages.getImage(asset.getCategory()));
			newItem.setData(asset);
		} else {
			// go up a level
			addNewAsset(asset.getParent());
			// The parent was added now check to add self.
			if(asset.getAssetUiItem() == null) {
				TreeItem newItem = new TreeItem(parent, SWT.NONE);
				asset.setAssetUiItem(newItem);
				newItem.setText(asset.getProjectsDisplayName());
				newItem.setImage(projectImages.getImage(asset.getCategory()));
				newItem.setData(asset);
			}
		}
	}
	@Override
	public void changeProjectName(AssetModelData project) {
		project.getAssetUiItem().setText(project.getProjectsDisplayName());
	}
	@Override
	public void asyncRemoveAssets(Collection<AssetModelData> removedAssets) {
		getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				removeAssets(removedAssets);
			}
		});
	}
	private void removeAssets(Collection<AssetModelData> removedAssets) {
		for(AssetModelData asset: removedAssets) {
			TreeItem item = asset.getAssetUiItem();
			
			if(item != null && ! item.isDisposed()) {
				item.dispose();
			}
		}
	}

	@Override
	public void removeAsset(AssetModelData asset) {
		TreeItem item = asset.getAssetUiItem();
		
		if(item != null && ! item.isDisposed()) {
			getDisplay().asyncExec(new Runnable() {
				@Override
				public void run() {
					item.dispose();
				}
			});
		}
	}

}
