package av.proj.ide.envServices;

import java.util.List;

import av.proj.ide.envServices.EnvBuildTargets.HdlPlatformInfo;
import av.proj.ide.envServices.EnvBuildTargets.RccPlatformInfo;

public interface BuildPlatformUpdate {
	public void addHdlPlatforms(List<HdlPlatformInfo> hdlPlatforms);
	public void removeHdlPlatforms(List<HdlPlatformInfo> hdlPlatforms);
	public void addRccPlatforms(List<RccPlatformInfo> rccPlatforms);
	public void removeRccPlatforms(List<RccPlatformInfo> rccPlatforms);
}
