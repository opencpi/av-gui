/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.envServices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.eclipse.ui.console.MessageConsole;
import org.json.simple.JSONObject;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.EnvironmentProject;
import av.proj.ide.assetModel.JsonProjectParser;
import av.proj.ide.assetModel.OpenCPICategory;
import av.proj.ide.assetServices.SyncLock;
import av.proj.ide.avps.internal.AvpsResourceManager;
import av.proj.ide.avps.internal.CommandExecutor;
import av.proj.ide.internal.AssetDetails.AuthoringModel;
import av.proj.ide.wizards.internal.CreateAssetFields;
import av.proj.ide.wizards.internal.CreateProjectFields;
import av.proj.ide.wizards.internal.CreateWorkerFields;
import av.proj.ide.wizards.internal.HdlPlatformFields;
/***
 * This class is working to optimize reading the environment since ocpidev show is taking
 * much longer. The intent is to use this class to construct the colleague classes used by 
 * AngryViperAssetService.  This was done quickly to make the UI startup more responsive
 * and to integrate with the current implementations to provide correct information and
 * not impact refresh processing.  All of this could use a refactoring touch. One of the core
 * issues faced with these classes was class size as the UI grew in capability.
 */
public class OpenCpiEnvSupportMethods {

		
	public OpenCpiEnvSupportMethods() {
    	compsLock.acquire();
    	workersLock.acquire();
		getOpenCpiProjects();
		loadEnvComponentsJsonData();
		loadEnvWorkersJsonData();
	}
	
	private List<String> registerCmd = new ArrayList<String>();
	private List<String> unregisterCmd = new ArrayList<String>();
	{
		registerCmd.add("ocpidev");
		registerCmd.add("-d");
		registerCmd.add(null);
		registerCmd.add("register");
		registerCmd.add("project");    
		
		unregisterCmd.add("ocpidev");
		unregisterCmd.add("-d");
		unregisterCmd.add(null);
		unregisterCmd.add("unregister");
		unregisterCmd.add("project");    
		unregisterCmd.add("-f");
	}
	
	public String getWorkersList() {
		if(workersList == null) {
			workersLock.acquire();
			workersLock.release();
		}
		return workersList;
	}
	public JSONObject getComponentsJson() {
		if(componentsJson == null) {
			compsLock.acquire();
			compsLock.release();
		}
		return componentsJson;
	}
	
	protected String [] getWorkersCmd = {"ocpidev", "show", "workers", "--simple"};
	String workersList = null;
	SyncLock workersLock = new SyncLock();
	protected void loadEnvWorkersJsonData() {
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
        		MessageConsole cons = AvpsResourceManager.getInstance().getNoticeConsoleInView();
        		StringBuilder errMessage = new StringBuilder();
        		workersList = CommandExecutor.getCommandResult(getWorkersCmd, cons, errMessage);
        		workersLock.release();
           }
        });
		loader.start();
	}
	
	protected String [] getComponentsCmd = {"ocpidev", "show", "components", "--json"};
	JSONObject componentsJson = null;
	SyncLock compsLock = new SyncLock();
	protected void loadEnvComponentsJsonData() {
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	componentsJson = EnvBuildTargets.getEnvInfo(getComponentsCmd);
            	compsLock.release();
            }
        });
		loader.start();
	}
	
	protected String [] getProjectsCmd = {"ocpidev", "show", "projects", "--json"};

	public TreeMap<String, EnvironmentProject> getOpenCpiProjects() {
		// This gets the registered projects. Unregistered Opencpi projects are not
		// picked up here.
		JSONObject jsonObject = EnvBuildTargets.getEnvInfo(getProjectsCmd);
		return JsonProjectParser.getOpenCpiProjects(jsonObject);
	}
	protected String [] deleteAssetCmd = {"ocpidev", "-d", null, "delete"};
	
	public boolean deleteAsset(AssetModelData asset, StringBuilder sb) {
		final List<String> command = new ArrayList<String>(getBaseCmd());
		command.set(2, asset.projectLocation.projectPath);
		command.set(3, "delete");
		boolean addLibraryName = false;
		AssetModelData library = null;

		switch(asset.category) {
		case application:
			command.add("application");
			break;
		case xmlapp:
			command.add("application");
			String name = asset.assetName.substring(0, asset.assetName.length() -4);
			command.add(name);
			command.add("-X");
			break;

		case assembly:
			command.add("hdl");
			command.add("assembly");
			break;
		case card:
			command.add("hdl");
			command.add("card");
			break;
		case component:
			library = asset.getParent().getParent();
			command.add("spec");
			addLibraryName = true;
			break;
		case componentsLibrary:
			command.add("library");
			break;
		case device:
			command.add("hdl");
			command.add("device");
			break;
//		case hdlLibrary:
//			break;
//		case hdlTest:
//			break;
		case library:
			command.add("library");
			break;
		case platform:
			command.add("hdl");
			command.add("platform");
			break;
		case primitive:
			command.add("hdl");
			command.add("primitive");
			command.add("library");
			break;
		case project:
			String projectPath = asset.projectLocation.projectPath;
			String[] pathParts = projectPath.split("/");
			String projectDir = pathParts[pathParts.length -1];
			String ParentDir = projectPath.substring(0, projectPath.length() - projectDir.length());
			command.set(2, ParentDir);
			command.add("project");
			command.add(projectDir);
			break;
		case protocol:
			library = asset.getParent().getParent();
			command.add("protocol");
			addLibraryName = true;
			break;
		case test:
			library = asset.getParent();
			command.add("test");
			addLibraryName = true;
			break;
		case worker:
			library = asset.getParent();
			command.add("worker");
			addLibraryName = true;
			break;
		default:
			break;
		}
		
		if(asset.category != OpenCPICategory.project && asset.category != OpenCPICategory.xmlapp)
			command.add(asset.assetName);
		
		if(addLibraryName) {
			if(library.category == OpenCPICategory.project) {
			command.add("-p");
			} else {
				command.add("-l");
			}
			command.add(library.assetName);
		}
		// Don't want the prompt back
		command.add("-f");
		
		MessageConsole cons = AvpsResourceManager.getInstance().getNoticeConsoleInView();
		//System.out.println(command.toString()); 
		boolean result = CommandExecutor.executeCommand(command, cons, sb);
		return result;
	}
	
	private static ArrayList<String> baseCmd = null;
	private static ArrayList<String> getBaseCmd() {
		if(baseCmd == null) {
			baseCmd = new ArrayList<String>(4);
			baseCmd.add("ocpidev");
			baseCmd.add("-d");
			baseCmd.add(null);
			baseCmd.add("create");
		}
		return baseCmd;
	}
	
	public boolean createAsset(CreateAssetFields assetElem, StringBuilder sb) {
		final List<String> command = new ArrayList<String>(getBaseCmd());
		OpenCPICategory type = assetElem.getType();
		switch(type) {

		case application:
			if (assetElem.isXmlOnly()) {
				command.add("-X");
			}
			break;
		case assembly:
			break;
		case platform:
			HdlPlatformFields platform = (HdlPlatformFields)assetElem;
			String partNumber = platform.getPartNumber();
			if(partNumber != null && ! partNumber.isEmpty()) {
				command.add("-g");
				command.add(partNumber);
			}
			String freq = platform.getTimerServer();
			
			if(freq != null && ! freq.isEmpty()) {
				command.add("-q");
				command.add(freq);
			}
			break;
		case primitive:
			break;
		case component:
			if (assetElem.isTopLevelSpec()) {
				command.add("-p");
			} else {
				String library = assetElem.getLibraryName();
				if (library != null) {
					command.add("-l");
					command.add(library);
				} 
			}
			
			break;
 		case protocol:
			if (assetElem.isTopLevelSpec()) {
				command.add("-p");
			} else {
				String library = assetElem.getLibraryName();
				if (library != null) {
					command.add("-l");
					command.add(library);
				} 
			}
			
			break;
		case library:
			break;
			
		case worker:
			CreateWorkerFields worker = (CreateWorkerFields)assetElem;
			int last = command.size() - 1;
			String name = command.get(last);
			if(worker.getModel() == AuthoringModel.HDL) {
				name += ".hdl";
			}
			else {
				name += ".rcc";
			}
			command.set(last, name);
			
			String library = assetElem.getLibraryName();
			if (library != null) {
				command.add("-l");
				command.add(library);
			}
			command.add("-S");
			command.add(worker.getComponentSpec());
			command.add("-L");
			command.add(worker.getLanguage());
			
			break;
			
		case test:
			library = assetElem.getLibraryName();
			if (library != null) {
				command.add("-l");
				command.add(library);
			}
			break;
			
		case project:
			command.add("--register");
			CreateProjectFields project = (CreateProjectFields)assetElem;
			if(project.getPackageName() != null) {
				command.add("-N");
				command.add(project.getPackageName());
			}
			if(project.getPrefix() != null) {
				command.add("-F");
				command.add(project.getPrefix());
			}
			Collection<String> deps = project.getDependencies();
			if(! deps.isEmpty()) {
				for(String dep : deps) {
					command.add("-D");
					command.add(dep);
				}
			}
			break;
			
		default:
			
		}
		MessageConsole cons = AvpsResourceManager.getInstance().getNoticeConsoleInView();
		return CommandExecutor.executeCommand(command, cons, sb);
	}


	public String getPackageId(File projFile) {
		File packageFile = new File(projFile, "package-id"); 
		if(! (packageFile.exists() && packageFile.isFile())) return null;
		String packageName = "";
		String line = null;
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileReader = new FileReader(packageFile);
			bufferedReader = new BufferedReader(fileReader);
			line = bufferedReader.readLine();
			if(line != null) {
				packageName = line; // +".";
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (fileReader != null) {
					fileReader.close();
				}
				if (bufferedReader != null) {
					bufferedReader.close();
				}
			} catch (IOException e) {
			}
		}
		return packageName;
	}

	public TreeSet<String> getAllProtocols(Collection<EnvironmentProject> projects) {
		TreeSet<String> protocols = new TreeSet<String>();
		for(EnvironmentProject project: projects) {
			File projectFolder = new File(project.projectPath);
			if (projectFolder != null && projectFolder.exists()) {
				File topSpecsDir = new File(projectFolder, "specs");
				getProtocols(topSpecsDir, protocols);
			}
		}
		return protocols;
	}
	
	public static void getProtocols(File specsDir, Set<String> protocols) {
		if(specsDir == null) return;
		if(specsDir.exists() && specsDir.isDirectory() ){
			String[] children = specsDir.list();
			for(String child : children) {
				File specFile = new File(specsDir, child);
				if(specFile.isFile()){
					String fileName = specFile.getName().toLowerCase();
					if (fileName.endsWith("prot.xml") ||fileName.endsWith("protocol.xml")) {
						protocols.add(specFile.getName().replace(".xml", ""));
					}
				}
			}
		}
	}
	
}
