/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.envServices;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.eclipse.ui.console.MessageConsole;
import org.json.simple.JSONObject;

import av.proj.ide.assetModel.AngryViperAsset;
import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.EnvironmentProject;
import av.proj.ide.assetModel.JsonProjectParser;
import av.proj.ide.assetModel.OcpiAssetFactory;
import av.proj.ide.avps.internal.AvpsResourceManager;
import av.proj.ide.avps.internal.CommandExecutor;
import av.proj.ide.envServices.EnvBuildTargets.BuildEnvDifferences;
import av.proj.ide.envServices.EnvBuildTargets.HdlPlatformInfo;
import av.proj.ide.envServices.EnvBuildTargets.HdlVendor;
import av.proj.ide.envServices.EnvBuildTargets.RccPlatformInfo;
import av.proj.ide.internal.UiComponentSpec;
import av.proj.ide.wizards.internal.CreateAssetFields;
/***
 * This class is working to optimize reading the environment since ocpidev show is taking
 * much longer. The intent is to use this class to construct the colleague classes used by 
 * AngryViperAssetService.  This was done quickly to make the UI startup more responsive
 * and to integrate with the current implementations to provide correct information and
 * not impact refresh processing.  All of this could use a refactoring touch. One of the core
 * issues faced with these classes was class size as the UI grew in capability.
 */
public class OpenCpiEnvironmentService implements IEnvironmentInfoService {

	public Collection<EnvironmentProject> getRegisteredProjects() {
		return projectLookup.values();
	}
	
	public static OpenCpiEnvironmentService getInstance() {
		if (instance == null) {
			instance = new OpenCpiEnvironmentService();
		}
		return instance;
	}
	
	public Collection<HdlPlatformInfo> getHdlPlatforms() {
		return envBuildInfo.getHdlPlatforms();
	}
	public Collection<RccPlatformInfo> getRccPlatforms() {
		return envBuildInfo.getRccPlatforms();
	}
	public Collection<HdlVendor> getHdlTargets() {
		return envBuildInfo.getVendors();
	}
	
	public void registerPlatformRefresh(BuildPlatformUpdate platformUpdater) {
		this.platformUpdater = platformUpdater;
	}
	private BuildPlatformUpdate platformUpdater = null;
	
	public boolean unregisterProject(AngryViperAsset asset, StringBuilder s) {
		unregisterCmd.set(2, asset.projectLocation.projectPath);
	    StringBuilder sb = new StringBuilder();
		MessageConsole consoleMsg = AvpsResourceManager.getInstance().getNoticeConsoleInView();
		boolean result = CommandExecutor.executeCommand(unregisterCmd, consoleMsg, sb);
		if(result) {
			EnvBuildTargets envBuild = new EnvBuildTargets();
			asset.projectLocation.setRegistered(false);
			BuildEnvDifferences diffs = envBuildInfo.getPlatformsRemoved(envBuild);
			if(diffs.arePlatformsRemoved() && platformUpdater != null) {
				platformUpdater.removeHdlPlatforms(diffs.getHdlPlaformsRemoved());
				platformUpdater.removeRccPlatforms(diffs.getRccPlaformsRemoved());
				envBuildInfo = envBuild;
			}
		}
        return result;
	}

	public boolean registerProject(AngryViperAsset asset, StringBuilder s) {
		
		registerCmd.set(2, asset.projectLocation.projectPath);
	    StringBuilder sb = new StringBuilder();
		MessageConsole consoleMsg = AvpsResourceManager.getInstance().getNoticeConsoleInView();
		boolean result = CommandExecutor.executeCommand(registerCmd, consoleMsg, sb);
		if(result) {
			EnvBuildTargets envBuild = new EnvBuildTargets();
			asset.projectLocation.setRegistered(true);
			BuildEnvDifferences diffs = envBuildInfo.getPlatformsAdded(envBuild);
			if(diffs.arePlatformsAdded()) {
				platformUpdater.addHdlPlatforms(diffs.getHdlPlaformsAdded());
				platformUpdater.addRccPlatforms(diffs.getRccPlaformsAdded());
				envBuildInfo = envBuild;
			}
		}
        return result;
	}
	private List<String> registerCmd = new ArrayList<String>();
	private List<String> unregisterCmd = new ArrayList<String>();
	{
		registerCmd.add("ocpidev");
		registerCmd.add("-d");
		registerCmd.add(null);
		registerCmd.add("register");
		registerCmd.add("project");    
		
		unregisterCmd.add("ocpidev");
		unregisterCmd.add("-d");
		unregisterCmd.add(null);
		unregisterCmd.add("unregister");
		unregisterCmd.add("project");    
		unregisterCmd.add("-f");
	}
	
	private EnvBuildTargets envBuildInfo;
	
	private static OpenCpiEnvironmentService instance = null;
	private OpenCpiEnvironmentService() {
    	envBuildInfo = new EnvBuildTargets();
    	supportMethods = new OpenCpiEnvSupportMethods();
    	projectLookup = supportMethods.getOpenCpiProjects();
	}
	private OpenCpiEnvSupportMethods supportMethods;
	private TreeMap<String, EnvironmentProject> projectLookup;

	public boolean deleteAsset(AssetModelData asset, StringBuilder sb) {
		boolean result = supportMethods.deleteAsset(asset, sb);
		return result;
	}
	
	public AssetModelData createAsset(CreateAssetFields assetElem, StringBuilder sb) {
		String projectName = assetElem.getProjectName();
		EnvironmentProject project = projectLookup.get(projectName);
		boolean r = supportMethods.createAsset(assetElem, sb);
		if(! r) return null;
		AssetModelData asset = OcpiAssetFactory.createNewAsset(assetElem, project);
		return asset;
	}
	
	/*****
	 *     IEnvironmentInfoService queries
	 */
	@Override
	public Map<String, EnvironmentProject> getRegisteredProjectsLessCore() {
		Map<String, EnvironmentProject> projects = new HashMap<String, EnvironmentProject>(projectLookup.size());
		for(EnvironmentProject project : projectLookup.values()) {
			if(project.packageId.equals("ocpi.core")) continue;
			projects.put(project.packageId, project);
		}
		return projects;
	}
	
	@Override
	public Map<String, EnvironmentProject> getOpenCpiWorkspaceProjects() {
		Map<String, EnvironmentProject> projects = new HashMap<String, EnvironmentProject>(projectLookup.size());
		for(EnvironmentProject project : projectLookup.values()) {
			if(project.isOpenCpiProject() && project.isOpenInEclipse()) projects.put(project.packageId, project);
		}
		return projects;
	}

	@Override
	public EnvironmentProject getProjectByEclipseName(String projectName) {
		for(EnvironmentProject project : projectLookup.values()) {
			if(project.getEclipseName().equals(projectName)) return project;
		}
		return null;
	}

	@Override
	public Collection<String> getProjectComponentLibraries(String projectId){
		JSONObject componentsJson = supportMethods.getComponentsJson();
    	JSONObject projectObj = JsonProjectParser.getProjectJson(componentsJson, projectId); 
    	Collection<String> libraries = JsonProjectParser.getLibraries(projectObj);
		return libraries;
	}
	
	@Override
	public Map<String, UiComponentSpec> getComponentsInLibrary(String projectId, String libraryId){
		JSONObject componentsJson = supportMethods.getComponentsJson();
		JSONObject projectObj = JsonProjectParser.getProjectJson(componentsJson, projectId); 
    	JSONObject libraryObj = JsonProjectParser.getLibraryJson(projectObj, libraryId); 
    	Map<String, UiComponentSpec> specs = JsonProjectParser.getSpecsInLibrary(libraryObj, libraryId);
		return specs;
	}

	@Override
	public String getComponentName(String specFileName) {
		if(specFileName.toLowerCase().endsWith("spec.xml")) {
			String componentName = specFileName.substring(0, specFileName.length() -9);
			return componentName;
		}
		return null;
	}

	Map<String, Map<String, UiComponentSpec>> allCompsByProject = null;
	@Override
	public Map<String, UiComponentSpec>  getComponentsAvailableToProject(String project) {
		EnvironmentProject proj = projectLookup.get(project);
		if(! proj.isRegistered()) return null;

		if(allCompsByProject == null) {
			allCompsByProject = constuctAllEnvComponents();
		}
		TreeMap<String, UiComponentSpec> comps = new TreeMap<String, UiComponentSpec>();
		ArrayList<String> deps = new ArrayList<String> (proj.dependencies);
		deps.add(proj.packageId);
		proj = projectLookup.get("ocpi.core");
		if(proj.isRegistered()) {
			deps.add("ocpi.core");
		}
		for(String pid : deps) {
			Map<String, UiComponentSpec> components = allCompsByProject.get(pid);
			comps.putAll(components);
		}
		return comps;
	}
	
	private Map<String, UiComponentSpec> allCompsByDisplayName = null;
	
	private Map<String, Map<String, UiComponentSpec>> constuctAllEnvComponents() {
		JSONObject compsObj = supportMethods.getComponentsJson();
		Map<String, Map<String, UiComponentSpec>> all = JsonProjectParser.getProjectObjects(compsObj);
		return all;
	}

	@Override
	public Map<String, UiComponentSpec> getApplicationComponents() {
		if(allCompsByProject == null) {
			allCompsByProject = constuctAllEnvComponents();
		}
		if(allCompsByDisplayName == null) {
			allCompsByDisplayName = new TreeMap<String, UiComponentSpec>();
			for(String pid : allCompsByProject.keySet()) {
				Map<String, UiComponentSpec> components = allCompsByProject.get(pid);
				allCompsByDisplayName.putAll(components);
			}
		}
		return allCompsByDisplayName;
	}
	
	@Override
	public UiComponentSpec getUiSpecByDisplayName(String specName) {
		getApplicationComponents();
		return allCompsByDisplayName.get(specName);
	}
	
	@Override
	public UiComponentSpec getUiSpecByFileInfo(String fullLibraryPath, String specFileName, String specFilePathname) {
		File libFile = new File(fullLibraryPath);
		String compName = getComponentName(specFileName);
		String libPackageId = supportMethods.getPackageId(libFile);
		UiComponentSpec spec = new UiComponentSpec(libPackageId,compName,  specFilePathname);
		return spec;
	}
	
	TreeSet<String> protocols = null;
	@Override
	public Collection<String> getProtocols() {
		if(protocols == null) {
			protocols = supportMethods.getAllProtocols(projectLookup.values());
		}
		return protocols;
	}
	
	TreeSet<String> hdlWorkers = null;
	@Override
	public Collection<String> getHdlWorkers() {
		if(hdlWorkers == null) {
			String workersList = supportMethods.getWorkersList();
			hdlWorkers = new TreeSet<String>();
			String[] outputLines = workersList.split(" ");
			for(String worker : outputLines) {
				if(worker.endsWith(".hdl")) {
					String ohadWorker = worker.substring(0, worker.length() -4);
					hdlWorkers.add(ohadWorker);
				}
			}
			
			
		}
		return hdlWorkers;
	}
	
}
