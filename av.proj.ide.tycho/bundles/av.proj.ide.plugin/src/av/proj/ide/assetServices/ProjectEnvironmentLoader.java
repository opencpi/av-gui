/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetServices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.json.simple.JSONObject;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.EnvironmentProject;
import av.proj.ide.assetModel.OcpiAssetFactory;
import av.proj.ide.envServices.EnvBuildTargets;
import av.proj.ide.envServices.OpenCpiEnvironmentService;

/***
 * This class is working to optimize reading the environment since ocpidev show is taking
 * much longer. The intent is to use this class to construct the colleague classes used by 
 * AngryViperAssetService.  This was done quickly to make the UI startup more responsive
 * and to integrate with the current implementations to provide correct information and
 * not impact refresh processing.  All of this could use a refactoring touch. One of the core
 * issues faced with these classes was class size as the UI grew in capability.
 */
public class ProjectEnvironmentLoader {

	TreeMap<String, ProjectModelController> openCpiProjects;

	private SyncLock loadLock = new SyncLock();
	public TreeMap<String, ProjectModelController> getInitialProjects() {
		loadLock.acquire();
		loadLock.release();
		return openCpiProjects;
	}

	ProjectEnvironmentLoader (ProjectViewUpdater receiver) {
		loadLock.acquire();
		openCpiProjects = new TreeMap<String, ProjectModelController>();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
        		load(receiver);
            }
        });
		loader.start();
	}
	
	Collection<EnvironmentProject> registeredProjects;
	ArrayList<IProject> eclipseProjects;
	List<EnvironmentProject> registeredProjectsNotInEclipse = null;
	
	protected String [] getProjectsCmd = {"ocpidev", "show", "projects", "--json"};
	private ArrayList<Thread> loadThreads = new ArrayList<Thread>();
	private Map<String, EnvironmentProject> projectLookup;

	
	void load(ProjectViewUpdater receiver) {
		getEnvComponentsJsonData();
		loadEclipseProjects();
		loadRegisteredProjects();
		waitOnThreads();
		boolean coreIsRegistered = false;
		projectLookup = new HashMap<String,EnvironmentProject>();
		for(EnvironmentProject project : registeredProjects) {
			projectLookup.put(project.getProjectPath(), project);
			if( "ocpi.core".equals(project.getPackageId()) ) {
				coreIsRegistered = true;
			}
		}
		
		for(IProject eProject: eclipseProjects) {
			IPath path = eProject.getLocation();
			String thePath = path.toOSString();
			if(projectLookup.containsKey(thePath)) {
				EnvironmentProject project = projectLookup.get(thePath);
				project.mergeElipseProjectData(eProject);
				if(project.isOpenCpiProject() && project.isOpenInEclipse()) {
					AssetModelData projectModel = OcpiAssetFactory.createProjectAsset(project);
					ProjectModelController modelControl = new ProjectModelController(projectModel, receiver);

					modelControl.loadSecondTierData();
					modelControl.loadFullProjectData();
					openCpiProjects.put(project.getPackageId(), modelControl);
	  			}
			}
			else {
				EnvironmentProject project = new EnvironmentProject(eProject);
				projectLookup.put(project.getProjectPath(), project);
		    	long start = System.currentTimeMillis();
				Thread loader = new Thread(new Runnable() {
		            public void run()
		            {
		            	project.checkForOpenCpiProject();
						project.mergeElipseProjectData(eProject);
						if(project.isOpenCpiProject() && project.isOpenInEclipse()) {
							AssetModelData projectModel = OcpiAssetFactory.createProjectAsset(project);
							ProjectModelController modelControl = new ProjectModelController(projectModel, receiver);

							modelControl.loadSecondTierData();
							modelControl.loadFullProjectData();
							addProject(project.getPackageId(), modelControl);
			  			}
		            }
		        });
				loadThreads.add(loader);
				loader.setName(start + " Checking " + project.getEclipseName());
				loader.start();
			}
		}
		waitOnThreads();
		
		for(EnvironmentProject project : registeredProjects) {
			if(coreIsRegistered) {
				project.addDependency("ocpi.core");
			}
			if( ! project.isOpenInEclipse()) {
				if(registeredProjectsNotInEclipse == null) {
					registeredProjectsNotInEclipse = new ArrayList<EnvironmentProject>();
				}
				registeredProjectsNotInEclipse.add(project);
			}
		}
		registeredProjects = null;
		eclipseProjects = null;
		loadLock.release();
	}
	
	private Object lock = new Object();
	private void addProject(String packageId, ProjectModelController modelControl) {
		synchronized(lock) {
			openCpiProjects.put(packageId, modelControl);
		}
	}
	
	protected String [] getComponentsCmd = {"ocpidev", "show", "components", "--json"};
	JSONObject componentsJson = null;
	protected void getEnvComponentsJsonData() {
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	componentsJson = EnvBuildTargets.getEnvInfo(getComponentsCmd);

            }
        });
		loader.start();
	}

	protected ArrayList<IProject> getEclipseWorkspaceProjects() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IProject [] projects = workspace.getRoot().getProjects();
		int count = projects.length;
		eclipseProjects = new ArrayList<IProject>(count);
		
		IProject project;
		for(int i= 0; i < count; i++) {
			project = projects[i];
			if(! project.isOpen()) continue;
			if("RemoteSystemsTempFiles".equals(project.getName())) continue;
			
			eclipseProjects.add(project);
		}
		return eclipseProjects;
	}
	
	protected void getOpenCpiProjects() {
		// This gets the registered projects. Unregistered Opencpi projects are not
		// picked up here.
		OpenCpiEnvironmentService srv = OpenCpiEnvironmentService.getInstance();
		registeredProjects = srv.getRegisteredProjects();
	}

	/***
	 * Internal concurrent loading.
	 */
	protected void waitOnThreads() {
        boolean done = false;
        while (!done)
        {
            try
            {
        		for(Thread loading : loadThreads) {
        			loading.join();
        			//System.out.println(loading.getName() + " finished " +System.currentTimeMillis());
        		}
                done = true;
                loadThreads.clear();
            }
            catch (InterruptedException e)
            {
    			//System.out.println("Got Interrupt");
            }
        }
        // Perform Second check.
		for(Thread loading : loadThreads) {
			try {
				loading.join();
			} catch (InterruptedException e) {
			}
			//System.out.println(loading.getName() + " finished " +System.currentTimeMillis());
		}
        
	}

	protected void loadEclipseProjects() {
    	long start = System.currentTimeMillis();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	getEclipseWorkspaceProjects();
            }
        });
		loadThreads.add(loader);
		loader.setName(start + " Eclipse Projects ");
		loader.start();
	}
	protected void loadRegisteredProjects() {
    	long start = System.currentTimeMillis();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	getOpenCpiProjects();
            }
        });
		loadThreads.add(loader);
		loader.setName(start + " Registered Projects ");
		loader.start();
	}
	

}
