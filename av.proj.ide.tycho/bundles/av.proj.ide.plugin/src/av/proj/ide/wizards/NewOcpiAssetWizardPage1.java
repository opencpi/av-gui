/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.wizards;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.EnvironmentProject;
import av.proj.ide.assetModel.OpenCPICategory;
import av.proj.ide.envServices.IEnvironmentInfoService;
import av.proj.ide.envServices.OpenCpiEnvironmentService;
import av.proj.ide.internal.UiComponentSpec;
import av.proj.ide.wizards.internal.CreateAssetFields;


public class NewOcpiAssetWizardPage1 extends WizardPage {
	Composite container;
	

	final String[] modelOptions = {"RCC", "HDL"};
	final String[] hdlLangOptions = {"VHDL"};
	final String[] rccLangOptions = {"C++", "C"};
	
	final String[] assetOptions = {"Project", "Library", "Component",
			                               "Worker", "Protocol","Application",
			                               "HDL Assembly", "HDL Primitive Library",
                                             "HDL Platform", "Unit Test"
                                            };
	private FormWidgets form;
	
	// Services for the OpenCPI Project view to relate
	// initial selection info to the wizard.
	private OpenCPICategory presentInitialWizard = null;
	private String initialLib  = null;
	private String initialSpec  = null;
	private String initialProjectSelected = null;
	//private String initialProjectName = null;
	private OpenCPICategory anticipatedSpecLocation = null;
	
	private AssetModelData initialAssetSelection = null;
	
	private IEnvironmentInfoService infoService;
	EnvironmentProject intialProject;
	// Used when brought up from eclipse project explorer.
	public void setInitialProjectName(String projectName) {
		intialProject = infoService.getProjectByEclipseName(projectName);
		initialProjectSelected = intialProject.getPackageId();
	}
	public void setInitialAssetWizard(OpenCPICategory selectedAssetType) {
		this.presentInitialWizard = selectedAssetType;
	}
	
	public void setInitialAssetSelection(AssetModelData initialSelection) {
		initialAssetSelection = initialSelection;
		initialProjectSelected = initialSelection.projectLocation.packageId;
		switch(initialSelection.category) {
		case cards:
			break;
		case component:
		case protocol:
			initialSpec = initialSelection.assetName;
			AssetModelData parent = initialSelection.getParent();
			if(parent.category == OpenCPICategory.topLevelSpecs) {
				anticipatedSpecLocation = OpenCPICategory.topLevelSpecs;
			}
			else {
				// It's a library component.
				initialLib = parent.getParent().assetName;
			}
			
			break;
		case specs:
		case test:
		case worker:
			initialLib = initialAssetSelection.getParent().assetName;
			break;
		case componentsLibrary:
		case library:
			initialLib = initialAssetSelection.assetName;
			break;
		case devices:
			break;
		case platform:
			break;
		case topLevelSpecs:
			anticipatedSpecLocation = initialSelection.category;
			break;
		default:
			break;
		
		}
	}


	void setProjectSelection(String projectName) {
		
		String[] projects = form.projectCombo.getItems();
		int idx = 0;
		for(String project : projects) {
			if(project.equals(projectName)) {
				form.projectCombo.select(idx);
				break;
			}
			idx++;
		}
	}
	
	Map<String, EnvironmentProject> openCpiProjects;
	void loadProjectCombo() {
		openCpiProjects = infoService.getOpenCpiWorkspaceProjects();
		
		int idx = 0;
		int selIdx = 0;
		for(String project : openCpiProjects.keySet()) {
			form.projectCombo.add(project);
			if(project.equals(initialProjectSelected)) {
				selIdx = idx;
			}
			idx++;
		}
		if(openCpiProjects.size() != 0)
			form.projectCombo.select(selIdx);
	}
	
	List<String> libraryOptions;
	
	public NewOcpiAssetWizardPage1(NewOcpiAssetWizard wizard, ISelection selection) {
		super("wizardPage");
		setTitle("Create a new OpenCPI Asset");
		setDescription("Select the asset type from the drop down and complete the form.");
		//ctx = new DataBindingContext();
		libraryOptions = new ArrayList<String>();
		infoService = OpenCpiEnvironmentService.getInstance();
	}

	// Flow of control:
	// 1. commands/NewAssetHandler instantiates NewOcpiAssetWizard then sets the Project.
	//    If not project was selected it is an empty String.
	// 2. Next WizardDialog.createContents is called (eclipse wizard code, this calls 
	//    addPages() and the screen is instantiated. The only selection in the initial
	//    screen is the assetType selection dropdown.
	// 3. User selects an asset.
	
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		form = new FormWidgets(container);
		GridLayout layout = new GridLayout(2, false);
		layout.verticalSpacing = 9;
		container.setLayout(layout);
		
		int width = 350;
		//width = new PixelConverter(assetSelection).convertWidthInCharsToPixels(25);
		
		// +++++++++++++++++
		form.assetType = new Label(container, SWT.NULL);
		form.assetType.setText("Asset Type:");
		GridData gd = new GridData(SWT.END, SWT.CENTER, false, false);
		form.assetType.setLayoutData(gd);
				
		form.assetSelection = new Combo(container, SWT.BORDER | SWT.READ_ONLY);
		gd = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
		gd.widthHint = width;
		form.assetSelection.setLayoutData(gd);
		
		loadAssetTypes();
		if(presentInitialWizard != null) {
			layoutPanel(presentInitialWizard);
			setAssetSelection(presentInitialWizard);
		}
		
		form.assetSelection.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				commandChanged(container);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		
		setControl(container);
		container.layout();
	}
	
	String helpMessage = null;
	
	@Override
	public void performHelp() {
		if(helpMessage != null) {
			MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Wizard Help", helpMessage);			
		}
	}

	private void setAssetSelection(OpenCPICategory cat) {
		String selection = null;
		switch(cat) {
		case application:
			selection = "Application";
			break;
		case assembly:
			selection = "HDL Assembly";
			break;
		case component:
			selection = "Component";
			break;
		case platform:
			selection = "HDL Platform";
			break;
		case primitive:
			selection = "HDL Primitive Library";
			break;
		case protocol:
			selection = "Protocol";
			break;
		case worker:
			selection = "Worker";
			break;
		case test:
			selection = "Unit Test";
			break;
		default:
			break;
		}
		if(selection != null) {
			int idx = form.assetSelection.indexOf(selection);
			form.assetSelection.select(idx);
		}
		
	}

	private void loadAssetTypes() {
		for(String asset : assetOptions) {
			form.assetSelection.add(asset);
		}
	}
	
	// S E T    U P    A S S E T     S C R E E N
	class MyPoint {
		int width;
		int height;
		boolean resize = false;
	}
	
	private MyPoint layoutPanel(String selectedAsset) {
		OpenCPICategory initialAsset = OpenCPICategory.getCategory(selectedAsset);
		return layoutPanel(initialAsset);
	}
	
	private static String stdHelpMessage = "Create the framework asset and associated framework directory and files. If applicable the asset XML file is created and opened in the respective asset editor.";

	// Easier to track the selection by the enum.
	private OpenCPICategory currentAsset = null;
	
	private MyPoint layoutPanel(OpenCPICategory assetCatgory) {
		currentAsset = assetCatgory;
		int width = 350;
		MyPoint dimension = new MyPoint();
		// Basic asset dimensions
		dimension.width = 550;
		dimension.height = 500;
		
		boolean needsBasePanel = form.needsBasePanel();
		boolean initialFormComplete = false;
		if(! needsBasePanel) {
			int len = form.getAssetName().length();
			if(len > 2) {
				initialFormComplete = true;
			}
		}
		
		clearStatus();
		setMessage(null);
		helpMessage = stdHelpMessage;
		String status = null;
		
		switch (assetCatgory) {
		
		case project:
			StringBuilder sb = new StringBuilder();
			sb.append("OpenCPI projects are registered with a Package-ID that fully identifies the project. ");
			sb.append("The project Package-ID is used to reference a project as a project dependency and it ");
			sb.append("becomes a part of lower level Package-Ids that identify lower level project assets.\n\n");
			
			sb.append("The project Package-ID is represented as: package-prefix.package-name. ");
			sb.append("The default project Package-ID is \"local.<project name>\".");
			
			form.clearAllAssetWidgets();
			setDescription("Add a new Project");
			setMessage("Create and register a new OpenCPI project. Press help (\"?\" below) for more information.", IMessageProvider.INFORMATION);
			helpMessage = sb.toString();
			form.addNewProjectGroup(width);
			
			dimension.resize = true;
			dimension.width = 650;
			dimension.height = 550;
			initialFormComplete = true;
			break;
		
		case application:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			setDescription("Add a new Application");
			break;
			
		case library:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			
			setDescription("Add a new library");
			form.assetLabel.setText("Library Name:");
			if(libraryOptions == null || libraryOptions.isEmpty()) {
				form.assetName.setText("components");
				initialFormComplete = true;
			}
			// This project already has a library, this is an additional
			// lib.  Leave the input blank.
			break;
			
		case assembly:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			
			setDescription("Add a new HDL Assembly");
			form.assetLabel.setText("Assembly Name:");
			break;
			
		case platform:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			
			setDescription("Generate a new HDL platfrom");
			form.assetLabel.setText("HDL Platform Name:");
		
			form.addHDLPlatfromGroup(width);
			dimension.height = 500;
			dimension.width  = 620;
			dimension.resize = true;
	
			break;
			
		case primitive:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			
			setDescription("Add a new HDL Primitive Library");
			form.assetLabel.setText("Primitive Lib Name:");
			break;
			
		case component:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			setDescription("Add a new Component");
			form.assetLabel.setText("Component Name:");
			status = loadComponentInputSelections(width);
			
			if(status != null) {
				updateStatus(status);
			}
			dimension.height = 500;
			dimension.resize = true;
			break;
			
		case protocol:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			setDescription("Add a new Protocol");
			form.assetLabel.setText("Protocol Name:");
			status = loadComponentInputSelections(width);

			if(status != null) {
				updateStatus(status);
			}
			dimension.height = 500;
			dimension.resize = true;
			break;
			
		case worker:
			form.clearAccessoryWidgets();
			if(needsBasePanel) {
				addBasicAssetInputs(width);
			}
			
			setDescription("Create a new Worker");
			form.assetLabel.setText("Worker Name:");
			addWorkerInputs(width);
			addLibraryDropdown(width);
			form.addSpecDropdown(550);
			
			status = loadWorkerInputSelections();
			if(status != null) {
				updateStatus(status);
				initialFormComplete = false;
			}
			else {
				// Check project registration.  
				String projectName = form.getProjectSelection();
				EnvironmentProject proj = openCpiProjects.get(projectName);
				if( ! proj.isRegistered()) {
					setMessage("Warning - this project is not registered.");
				}
			}
			dimension.height = 600;
			dimension.width  = 750;
			dimension.resize = true;
			
			break;

		case test:
			form.clearAccessoryWidgets();
			if(form.assetName != null) {
				form.assetLabel.dispose();
				form.assetName.dispose();
				form.assetName = null;
			}
			setDescription("Add a Unit Test");
			form.addUnitTestInputs(width);
			addBaseSelections();
			
			status = loadTestInputSelections();
			if(status != null) {
				updateStatus(status);
				initialFormComplete = false;
			}
			else {
				initialFormComplete = true;
			}
			dimension.height = 450;
			dimension.resize = true;
			break;

		default:
			break;
		}
		this.setPageComplete(initialFormComplete);
		return dimension;
	}

	void loadLibraryOptions() {
		int idx = 0;
		int libIdx = -1;
		if(libraryOptions.size() == 0) return;
		for (String s : libraryOptions) {
			form.libraryCombo.add(s);
			if(s.equals(initialLib)) {
				libIdx = idx;
				break;
			}
			idx++;
		}
		if(libIdx != -1) {
			form.libraryCombo.select(libIdx);
		}
		else {
			form.libraryCombo.select(0);
		}
	}
	
	void reloadLibOptions() {
		boolean libraryAsset = false;
		switch(currentAsset) {
		default:
			break;
		case component:
		case protocol:
			// it didn't work well to try and update the 
			// destination radio group when the project selection
			// changes.  Just go ahead and reconstruct the wizard.
			commandChanged(container);
			break;
			
		case worker:
		case test:
			libraryAsset = true;
			break;
		}
		
		if(libraryAsset) {
			form.libraryCombo.removeAll();
			if(libraryOptions.size() == 0) {
				updateStatus("A components library must be created before creating this asset.");
			}
			else {
				loadLibraryOptions();
			}
		}
	}
	
	// The spec combo box needs to reflect the correct specs list for
	// workers and unit tests. This is a consideration when the project
	// or library selection changes.
	String updateSpecCombo(String project) {
		if(form.specCombo == null || form.specCombo.isDisposed())
			return null;
		
		form.specCombo.removeAll();
		
		switch(currentAsset) {
		default:
			break;
		case worker:
			Map<String, UiComponentSpec>  specs = infoService.getComponentsAvailableToProject(project);
			return loadSpecCombo(specs);
		case test:
			String selectedLibrary = form.libraryCombo.getText();
			String projectName = form.getProjectSelection();
			Map<String, UiComponentSpec> comps = infoService.getComponentsInLibrary(projectName, selectedLibrary);
			return loadSpecCombo(comps);
		}
		
		return null;
	}
	
	
	String loadComponentInputSelections(int inputWidth) {
		if(libraryOptions.size() == 0) {
			form.addToplevelSpecsDefault(inputWidth);
			return null;
		}
		
		if(libraryOptions.size() > 1) {
			form.addRadioGroupForLibs(inputWidth);
			addLibraryDropdown(inputWidth);
			loadLibraryOptions();
		}
		else {
			// Single lib project
			String lib = libraryOptions.get(0);
			form.addRadioGroupForSingleLib(inputWidth, lib);
			if(OpenCPICategory.topLevelSpecs == anticipatedSpecLocation) {
				form.topSpecsButton.setSelection(true);
				form.libButton.setSelection(false);
			}
		}
		return null;
	}

	String loadTestInputSelections() {
		String projectName = form.getProjectSelection();
		if(libraryOptions.size() == 0) {
			return "A components library must be created before creating this asset.";
		}
		loadLibraryOptions();
		int idx = form.libraryCombo.getSelectionIndex();
		if(idx > -1) {
			String selectedLibrary = form.libraryCombo.getItem(idx);
			Map<String, UiComponentSpec> comps = infoService.getComponentsInLibrary(projectName, selectedLibrary);
			return loadSpecCombo(comps);
		}
		return null;
	}
	
	Map<String, UiComponentSpec> compSelections = null;
	String loadSpecCombo (Map<String, UiComponentSpec> specs) {
		if(specs == null || specs.isEmpty()) {
			compSelections = null;
			return "There are no available components in this library.";
		}
		compSelections = specs;
		String specName = null;
		boolean setSpec = false;
		if(initialSpec != null) {
			specName = infoService.getComponentName(initialSpec);
			setSpec = true;
		}
		int idx = 0;
		int specIdx = -1;
		for(UiComponentSpec comp : specs.values()) {
			String name = comp.getDisplayName();
			form.specCombo.add(name);
			if(setSpec && name.contains(specName)) {
				specIdx = idx;
				break;
			}
			idx ++;
		}
		if( specIdx != -1) {
			form.specCombo.select(specIdx);
		}
		else if (specs.size() >= 1) {
			form.specCombo.select(0);
		}
		return null;
		
	}
	
	// HERE
	String loadWorkerInputSelections() {
		String projectName = form.getProjectSelection();
		if(libraryOptions.size() == 0) {
			return "A components library must be created before creating this asset.";
		}
		loadLibraryOptions();
		Map<String, UiComponentSpec>  specs = infoService.getComponentsAvailableToProject(projectName);
		if(specs == null) {
			return"There are no available components for this project.";
		}
		loadSpecCombo(specs);
		return null;
	}

	// *********************************************
	//                Form Layouts
	// *********************************************
	
	private boolean initialCommand = true;

	//this function detect the asset selection from Asset Type drop down and
	//set the layout panel.
	private void commandChanged(Composite container) {
		
		form.selectedAsset = form.getAssetSelection();
		MyPoint p;
		if(initialCommand) {
			p = layoutPanel(form.selectedAsset);
			p.resize = true;
		} else {
			p = layoutPanel(form.selectedAsset);
		}
		
		initialCommand = false;
		if(p.resize) {
			getShell().setSize(p.width, p.height);
		}
		container.layout();
		
	}
	
	void addBasicAssetInputs(int inputWidth) {
		// Add basic asset Name validation.
		// TODO: Need a way to control setting
		// page complete when other inputs require
		// validation.
		form.addBasicAssetInputs(inputWidth);
		addBaseSelections();
		NewOcpiAssetWizardPage1 me = this;
		form.assetName.addModifyListener( new ModifyListener() {
			boolean displayedMessage = false;
			@Override
			public void modifyText(ModifyEvent e) {
		       String currentText = ((Text)e.widget).getText();
				if (currentText.length() > 2) {
					boolean bad = namePattern.matcher(currentText).find();
					if(bad) {
						me.updateStatus("Invalid characters used in asset name.");
						displayedMessage = true;
					}
					else {
						me.setPageComplete(true);
						if(displayedMessage) {
							setErrorMessage(null);
							displayedMessage = false;
						}
					}
				}
				else {
					me.setPageComplete(false);
				}
			}
		});
	}
	
	// Regex notes: \p{Punct} - build it for all punctuation characters (backslash is escaped),
	// and not underscore, dash or period; dash and period escaped.
	private Pattern namePattern = Pattern.compile("[\\p{Punct} && [^_\\-]]");
	
	void addBaseSelections() {
		loadProjectCombo();
		getLibraryOptions();

		form.projectCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				// The destination project changed.
				setErrorMessage(null);
				String[] priorOptions = libraryOptions.toArray(new String[libraryOptions.size()]);
				getLibraryOptions();
				int delta = priorOptions.length - libraryOptions.size();
				boolean notEqual = false;
				// Make sure they are the same
				if(delta == 0) {
					for(String lib : priorOptions) {
						if( ! libraryOptions.contains(lib) ) {
							notEqual = true;
							break;
						}
					}
					if(notEqual) {
						reloadLibOptions();
					}
				}
				else {
					reloadLibOptions();
				}
				initialProjectSelected = form.getProjectSelection();
				
				// If a worker or a unit test is selected, must update
				// the spec list.
				String status = updateSpecCombo(initialProjectSelected);
				if(status != null) {
					updateStatus(status);
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {/* DO NOTHING*/}
		});	
	}


	void addLibraryDropdown(int inputWidth) {
		form.addLibraryDropdown(inputWidth);
		form.libraryCombo.addSelectionListener(new SelectionListener() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// Unit tests specs must be be updated if the library
			// changes. 
			if(currentAsset != OpenCPICategory.test) return;
			String project = form.getProjectSelection();
			String status = updateSpecCombo(project);
			if(status != null) {
				updateStatus(status);
			}
		}
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {/* DO NOTHING*/}
		});	
		
	}
	
	void addWorkerInputs(int inputWidth) {
		form.addWorkerInputs(inputWidth);
		
		for(String option : modelOptions) {
			form.modelCombo.add(option);
		}
		form.modelCombo.select(0);
		for(String opt : rccLangOptions) {
			form.languageCombo.add(opt);
		}
		form.languageCombo.select(0);
		
		form.modelCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				String selection = form.modelCombo.getItem(form.modelCombo.getSelectionIndex());
				form.languageCombo.removeAll();
				if("RCC".equals(selection)) {
					for(String opt : rccLangOptions) {
						form.languageCombo.add(opt);
					}
				}
				else {
					for(String opt : hdlLangOptions) {
						form.languageCombo.add(opt);
					}
				}
				form.languageCombo.select(0);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				
			}
		});
		
	}

	// *******************************************
	//             General Support Methods
	// *******************************************
	
	int getLibraryOptions() {
		libraryOptions.clear();
		String projectName = form.getProjectSelection();
		Collection<String>libraries = infoService.getProjectComponentLibraries(projectName);
		for(String lib : libraries) {
			libraryOptions.add(lib);
		}
		return libraries.size();
	}
	
//	private void updateLibraryOptionCombo() {
//		if (this.libraryOptionNameCombo != null && ! this.libraryOptionNameCombo.isDisposed()) {
//			this.libraryOptionNameCombo.removeAll();
//			for (String s : libraryOptions) {
//				this.libraryOptionNameCombo.add(s);
//			}
//			this.libraryOptionNameCombo.select(0);
//		}
//		else {
//			createLibraryOptionGroup();
//		}
//	}
	
	void clearStatus() {
		setErrorMessage(null);
		setPageComplete(false);
	}
	
	// validators use this.
	public void updateStatus(String message) {
//		for (Object b : ctx.getBindings().toArray()) {
//			((Binding)b).validateModelToTarget();
//		}
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	public CreateAssetFields getUsersRequest() {
		return form.getUsersRequest();
	}
	

}