AV-GUI plug-in is deprecated as of OpenCPI v2.3.4. It is replaced by the OpenCPI GUI found at https://gitlab.com/opencpi/ie-gui. 

This is the source distribution of the AV GUI, an IDE for OpenCPI, which is hosted on gitlab.com.
Installation instructions can be found in section 3.4 of the OpenCPI Installation Guide at https://opencpi.gitlab.io/releases/v2.2.1/docs/OpenCPI_Installation_Guide.pdf. 

OpenCPI is Open Source Software, licensed with the LGPL3.  See LICENSE.txt.
