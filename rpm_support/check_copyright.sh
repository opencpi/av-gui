#!/bin/bash --noprofile
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# This script will check compared to dedicated white list
# It needs to know the location of the copyright script

if [ -z "${OPENCPI_REPO}" ]; then
  echo "Need to set OPENCPI_REPO to base of main repo checkout"
  exit 99
fi

if [ ! -x "${OPENCPI_REPO}/scripts/copyright/copyright.py" ]; then
  echo "Could not find executable at ${OPENCPI_REPO}/scripts/copyright/copyright.py"
  exit 98
fi

function cleanup() {
  rm -rf copyright.log missing.log missing_diff.log
}

trap cleanup EXIT

OCPI_SCAN_COPYRIGHTS=x ${OPENCPI_REPO}/scripts/copyright/copyright.py
grep ":ERROR:" copyright.log | cut -f5 -d: | egrep -v '^\s*./app1/' | egrep -v '^\s*./ocpidev/' | sort  > missing.log
# Put a single space before each whitelist item to match the diff output.
# That space is needed or the later greps will see "---" as an issue.
diff -u <( sed -e 's/^/ /' rpm_support/copyright_whitelist) missing.log > missing_diff.log
RET=$?

if [ "${RET}" == "1" ]; then
  if egrep -q '^\+ ' missing_diff.log; then
    echo ""
    echo "The following file(s) need to have their copyright clause added"
    echo "or put into the whitelist."
    egrep '^\+ ' missing_diff.log | cut -c3-
  fi
  if egrep -q '^\- ' missing_diff.log; then
    echo ""
    echo "The following file(s) were listed as exemptions but no longer exist:"
    egrep '^\- ' missing_diff.log | cut -c3-
  fi
  exit 10 # Arbitrary non-0/1 value for Jenkins to decide we are unstable
fi

echo "All expected files have copyright block set."
