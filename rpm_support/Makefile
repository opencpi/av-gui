# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# This is the Makefile to build RPMs for ANGRYVIPER IDE (copied from the main project and tweaked)
RPM_BASENAME:=angryviper-ide

GIT_BRANCH:=$(notdir $(shell git name-rev --name-only HEAD 2>/dev/null))
# Might be empty (on Jenkins)
ifneq ($(filter $(MAKECMDGOALS),ide),)
  ifeq "$(GIT_BRANCH)" ''
    GIT_BRANCH:=$(shell cat jenkins_gitbranch)
  endif
  ifeq "$(GIT_BRANCH)" ''
    $(error Could not determine GIT_BRANCH. Either this is not a 'git' repository or you are not running on Jenkins)
  endif
endif

# Find out if we are a git version branch explicitly
# (Strictly "v" followed only by numbers or '.' with NO suffix)
GIT_VERSION:=$(shell echo $(GIT_BRANCH) | perl -ne '/^v[\.\d]+$$/ && print')
# For testing:
# GIT_VERSION:=$(shell echo v1.2.0 | perl -ne '/^v[\.\d]+$$/ && print')
RPM_RELEASE?=snapshot
COMMIT_HASH=$(shell git rev-list --max-count=1 HEAD 2>/dev/null)
# Might be empty (on Jenkins)
ifneq ($(filter $(MAKECMDGOALS),ide),)
  ifeq "$(COMMIT_HASH)" ''
    COMMIT_HASH:=$(shell cat jenkins_githash)
  endif
  ifeq "$(COMMIT_HASH)" ''
    $(error Could not determine COMMIT_HASH. Either this is not a 'git' repository or you are not running on Jenkins)
  endif
endif

# FYI - "COMMIT_TAG" doesn't actually have commit hash any more, but wanted to minimize changes in specfiles
COMMIT_TAG=%{nil}
ifeq "$(GIT_VERSION)" ''
  # Get Jenkins number in there too
  ifneq "$(BUILD_NUMBER)" ""
    COMMIT_TAG+=_J$(BUILD_NUMBER)
  endif
  ifneq "$(GIT_BRANCH)" "undefined"
    ifneq "$(GIT_BRANCH)" "develop"
      COMMIT_TAG+=_$(shell echo $(GIT_BRANCH)|tr - _)
    endif
  endif
  # Generate an increasing number for version comparisons (since git hashes aren't)
  # Number of 6-minute increments since release in 5 digits (rolls over >1/yr)
  RELEASE_TAG=_$(shell printf %05d $(shell expr `date -u +"%s"` / 360 - 4273900))
  RPM_VERSION:=1.5.0
  # When updating this, there are many XML files, etc. See AV-3986 for example.
else
  # This is a specific version branch
  RELEASE_TAG=%{nil}
  RPM_VERSION:=$(GIT_VERSION)
endif

# There are various targets...
.PHONY: help
.SILENT: help

# Default
help:
	printf "This is the Makefile to build IDE RPM for ANGRYVIPER\n\n"
	echo "The valid targets are:"
	echo " clean                 - Remove generated files from this directory"
	echo " cleanrpmbuild         - Cleans ~/rpmbuild/ (runs EVERY BUILD for consistency)"
	echo ""
	echo " ide                   - The IDE RPM"

include myhostname.mk

.PHONY: clean cleanrpmbuild
.SILENT: clean cleanrpmbuild
.IGNORE: clean cleanrpmbuild
clean: myhostnameclean
	grep -v "^#" .gitignore | xargs -ri sh -c 'rm -rf {}'

cleanrpmbuild:
	rm -rf ~/rpmbuild/*
	mkdir -p ~/rpmbuild/SOURCES/

TAR_BASE=$(RPM_BASENAME)-$(RPM_VERSION)
PWD:=$(shell pwd)

.PHONY: ide
.SILENT: ide

ide: cleanrpmbuild libmyhostname angryviper_eclipse_temp.tar
	echo Building IDE RPM...
	cp angryviper_eclipse_temp.tar ocpigui.desktop ~/rpmbuild/SOURCES
	set -o pipefail && $(SPOOF_HOSTNAME) rpmbuild -ba angryviper-ide.spec \
	--define="COMMIT_HASH $(COMMIT_HASH)" \
	--define="COMMIT_TAG $(shell echo "$(COMMIT_TAG)" | tr -d ' ')" \
	--define="RELEASE_TAG $(RELEASE_TAG)" \
	--define="RPM_BASENAME ${RPM_BASENAME}" \
	--define="RPM_RELEASE $(RPM_RELEASE)" \
	--define="RPM_VERSION ${RPM_VERSION}" \
	2>&1 | tee build-rpm.log
	cp -v ~/rpmbuild/RPMS/*/$(RPM_BASENAME)-* .
