# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

%define  _prefix /opt/opencpi/gui
Name:    %{RPM_BASENAME}
Version: %{RPM_VERSION}
Release: %{RPM_RELEASE}%{?RELEASE_TAG}%{?COMMIT_TAG}%{?dist}
BuildArch: x86_64
Prefix: %{_prefix}

Summary:   The ANGRYVIPER Graphical Editor
Group:     Applications/Engineering

License:        LGPLv3+
Source:         angryviper_eclipse_temp.tar
Source1:        ocpigui.desktop
Vendor:         ANGRYVIPER Team
Packager:       ANGRYVIPER team <REDACTED>

# Desktop stuff
BuildRequires: desktop-file-utils
Requires: oxygen-icon-theme
# Need user "opencpi" from OpenCPI:
Requires(pre): opencpi
# Need "ocpidev" from devel:
Requires: opencpi-devel
# CentOS on the training laptops only had 1.7; java-1.8.0-openjdk is what I used:
Requires: jre >= 1.8

# For AV-4231, started with a fresh Docker container...
# Make it look nicer, but not in C6: Requires: PackageKit-gtk3-module libcanberra-gtk3

# See fedora-devel-java-list discussion in September 2008.
# Prevent brp-java-repack-jars from being run.
%global __jar_repack %{nil}

# You can't say "kernel-devel" = "your installed kernel" (AV-2407)
#Requires: gcc-c++ kernel-devel
#Requires: chkconfig initscripts
#Requires(post): gcc-c++ kernel-devel
#Requires(post,preun): chkconfig initscripts
#Requires(pre): perl shadow-utils

%description
This package installs a copy of the Open Source Eclipse IDE with
various plug-ins, including the ANGRYVIPER one.
%if "0%{?COMMIT_HASH}"
Release ID: %{COMMIT_HASH}
%endif

%prep
%setup -q -n eclipse

%install
# Make tree
mkdir -p %{buildroot}/%{_prefix}
mkdir -p %{buildroot}/usr/bin/
# Move file
mv * %{buildroot}/%{_prefix}/
# Symlink
ln -sf %{_prefix}/eclipse %{buildroot}/usr/bin/ocpigui
# Install desktop file
install -m644 -D %{SOURCE1} $RPM_BUILD_ROOT/usr/share/applications/%{name}.desktop
desktop-file-validate $RPM_BUILD_ROOT/usr/share/applications/%{name}.desktop

%files
%defattr(-,opencpi,opencpi,-)
%dir %{_prefix}
%{_prefix}/
%doc %{_prefix}/readme/*
/usr/bin/ocpigui
/usr/share/applications/%{name}.desktop
