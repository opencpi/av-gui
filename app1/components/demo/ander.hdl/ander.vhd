-- THIS FILE WAS ORIGINALLY GENERATED ON Thu Jan 12 07:36:30 2017 EST
-- BASED ON THE FILE: ander.xml
-- YOU *ARE* EXPECTED TO EDIT IT
-- This file initially contains the architecture skeleton for worker: ander

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of ander_worker is
  signal idata_vld : bool_t;
  signal odata_vld : bool_t;
begin
  -----------------------------------------------------------------------------
  -- Take (when both upstream Workers and downstream Worker ready)
  -----------------------------------------------------------------------------
  in1_out.take <= in1_in.ready and in2_in.ready and ctl_in.is_operating and out_in.ready;
  in2_out.take <= in1_in.ready and in2_in.ready and ctl_in.is_operating and out_in.ready;

  idata_vld <= ctl_in.is_operating and out_in.ready and in1_in.ready and in2_in.ready and in1_in.valid and in2_in.valid;
  -----------------------------------------------------------------------------
  -- Enable all bytes on the stream
  -----------------------------------------------------------------------------  
  out_out.byte_enable <= (others => '1');
  -----------------------------------------------------------------------------
  -- Data
  -- AND the RAMP and SQUARE pulse together on the lower 16-bits
  -- Let the RAMP data pass through on the upper 16-bits
  -----------------------------------------------------------------------------
  and_low_pass_high : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        out_out.data <= (others => '0');
      elsif (idata_vld = '1') then
        out_out.data(15 downto 0) <= in1_in.data and in2_in.data;
        out_out.data(31 downto 16) <= in1_in.data;
      end if; 
      odata_vld <= idata_vld;
      -----------------------------------------------------------------------------
      -- Pass the RAMP's SOM/EOM out
      -----------------------------------------------------------------------------
      out_out.som <= in1_in.som;
      out_out.eom <= in1_in.eom;  
    end if;
  end process and_low_pass_high;

  -----------------------------------------------------------------------------
  -- Valid Output (when output data is valid (which includes upstream==ready))
  -----------------------------------------------------------------------------
  out_out.valid <= odata_vld;
  -----------------------------------------------------------------------------
  -- Give (when downstream Workers ready and our output data is valid)
  -----------------------------------------------------------------------------
  out_out.give <= odata_vld and ctl_in.is_operating and out_in.ready;

end rtl;
