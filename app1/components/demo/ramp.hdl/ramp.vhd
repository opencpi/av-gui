-- THIS FILE WAS ORIGINALLY GENERATED ON Wed Jan 11 10:32:01 2017 EST
-- BASED ON THE FILE: ramp.xml
-- YOU *ARE* EXPECTED TO EDIT IT
-- This file initially contains the architecture skeleton for worker: ramp

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of ramp_worker is
  signal idata_vld : bool_t;
  signal odata_vld : bool_t;
  signal buff_data : std_logic_vector(15 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Valid Input (when up/downstream Workers ready and upstream is valid
  -----------------------------------------------------------------------------  
  idata_vld <= ctl_in.is_operating and in_in.ready and out_in.ready and in_in.valid;

  -----------------------------------------------------------------------------
  -- Take (when upstream Workers ready and valid)
  -----------------------------------------------------------------------------
  in_out.take <= ctl_in.is_operating and in_in.ready and in_in.valid and out_in.ready;

  -----------------------------------------------------------------------------
  -- Valid Output (when "Give")
  -----------------------------------------------------------------------------
  out_out.valid <= odata_vld;
  
  ramp : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        buff_data <= (others => '0');
        odata_vld <= '0';
      elsif (idata_vld = '1') then
        buff_data <= std_logic_vector(signed(in_in.data) + signed(buff_data));
        odata_vld <= '1';
      else
        odata_vld <= '0';
      end if;
    end if;
  end process ramp;

  out_out.data <= buff_data;
  
  -----------------------------------------------------------------------------
  -- Give (when downstream Workers ready & valid)
  -----------------------------------------------------------------------------
  out_out.give <= out_in.ready and odata_vld and ctl_in.is_operating;
  
  -----------------------------------------------------------------------------
  -- Pass the SOM/EOM out
  -----------------------------------------------------------------------------
  out_out.som <= in_in.som;
  out_out.eom <= in_in.eom;

end rtl;
