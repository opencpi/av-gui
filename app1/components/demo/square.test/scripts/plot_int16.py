from __future__ import division
import numpy as np
import math
import matplotlib.pyplot as plt
import sys

from math import *

def main():

    print 'Number of arguments:', len(sys.argv), 'arguments.'
    print 'Argument List:', str(sys.argv)

    if len(sys.argv) < 1:
        print("Usage expected:\nfilename\n")
        sys.exit()

    #############################################################
    # Get input arguments
    #############################################################
    filename = sys.argv[1]

    ifd=open(filename,"r")
    data=np.fromfile(ifd,dtype=np.int16)
    ifd.close()

    plt.figure(1)
    plt.plot(data)
    plt.title("Output Data")
    plt.grid()

    plt.show()

main()
